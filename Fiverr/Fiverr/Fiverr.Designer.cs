﻿namespace Fiverr
{
    partial class Fiverr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.btnScrap = new System.Windows.Forms.Button();
            this.lblGigDataStart = new System.Windows.Forms.Label();
            this.lblGigDataEnd = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.fiverr_timer = new System.Windows.Forms.Timer(this.components);
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(12, 12);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.Size = new System.Drawing.Size(855, 383);
            this.webBrowser1.TabIndex = 2;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // btnScrap
            // 
            this.btnScrap.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnScrap.Location = new System.Drawing.Point(701, 478);
            this.btnScrap.Name = "btnScrap";
            this.btnScrap.Size = new System.Drawing.Size(93, 29);
            this.btnScrap.TabIndex = 3;
            this.btnScrap.Text = "Start Scrap";
            this.btnScrap.UseVisualStyleBackColor = true;
            this.btnScrap.Click += new System.EventHandler(this.btnScrap_Click);
            // 
            // lblGigDataStart
            // 
            this.lblGigDataStart.AutoSize = true;
            this.lblGigDataStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGigDataStart.ForeColor = System.Drawing.Color.Red;
            this.lblGigDataStart.Location = new System.Drawing.Point(9, 408);
            this.lblGigDataStart.Name = "lblGigDataStart";
            this.lblGigDataStart.Size = new System.Drawing.Size(99, 16);
            this.lblGigDataStart.TabIndex = 4;
            this.lblGigDataStart.Text = "lblGigDataStart";
            // 
            // lblGigDataEnd
            // 
            this.lblGigDataEnd.AutoSize = true;
            this.lblGigDataEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGigDataEnd.ForeColor = System.Drawing.Color.Red;
            this.lblGigDataEnd.Location = new System.Drawing.Point(9, 435);
            this.lblGigDataEnd.Name = "lblGigDataEnd";
            this.lblGigDataEnd.Size = new System.Drawing.Size(96, 16);
            this.lblGigDataEnd.TabIndex = 5;
            this.lblGigDataEnd.Text = "lblGigDataEnd";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(800, 478);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(67, 29);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // fiverr_timer
            // 
            this.fiverr_timer.Tick += new System.EventHandler(this.fiverr_timer_Tick);
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.AutoSize = true;
            this.lblErrorMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMessage.ForeColor = System.Drawing.Color.Red;
            this.lblErrorMessage.Location = new System.Drawing.Point(9, 462);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(108, 16);
            this.lblErrorMessage.TabIndex = 9;
            this.lblErrorMessage.Text = "lblErrorMessage";
            // 
            // Fiverr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(879, 519);
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblGigDataEnd);
            this.Controls.Add(this.lblGigDataStart);
            this.Controls.Add(this.btnScrap);
            this.Controls.Add(this.webBrowser1);
            this.Name = "Fiverr";
            this.Text = "Fiverr";
            this.Load += new System.EventHandler(this.Fiverr_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button btnScrap;
        private System.Windows.Forms.Label lblGigDataStart;
        private System.Windows.Forms.Label lblGigDataEnd;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Timer fiverr_timer;
        private System.Windows.Forms.Label lblErrorMessage;
    }
}

