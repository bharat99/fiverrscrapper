﻿using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using HtmlAgilityPack;
using System.Diagnostics;
using Fiverr.FiverrBAL;
using System.Text.RegularExpressions;

namespace Fiverr
{
    public partial class Fiverr : Form
    {
        ScrapeHelper _scrapeHelper = new ScrapeHelper(Convert.ToInt16(ConfigurationSettings.AppSettings["retryCount"].ToString()));
        FiverrBAL.FiverrBussiness _objFiverrBussiness = new FiverrBAL.FiverrBussiness(ConfigurationSettings.AppSettings["FiverrConnectionStringSql"].ToString());
        int page = Convert.ToInt32(ConfigurationSettings.AppSettings["PageSize"].ToString());
        int gigPerPage = Convert.ToInt32(ConfigurationSettings.AppSettings["GigPerPage"].ToString());
        int totalPage = 0;
        string SearchPageContent = string.Empty;
        string ServicesAvailable = string.Empty;
        string keyword = string.Empty;
        string GroupName = string.Empty;
        string keywordID = string.Empty;
        DataTable dtFiverrRawData = new DataTable();
        bool isComplete = false;
        bool isLastScrapGig = false;
        bool isKeywordRestart = false;
        bool isKeywordGroup = false;
        bool isScrapLastKeyword = false;
        bool isScrapGigDetails = false;
        DataTable dtKeyword = new DataTable();
        List<FiverrKeyword> listKeyword = new List<FiverrKeyword>();
        DataTable dtKeywordGigProcessed = new DataTable();
        List<FiverrKeyword> listKeywordGigProcessed = new List<FiverrKeyword>();
        bool isLastKeyword = false;
        int keywordLength;
        int keywordPosition = -1;
        int dataExtractCount;
        string Key_Remark; string Key_Status;
        string Currency = string.Empty;
        string Content=string.Empty;
        #region fiverr log process
        List<FiverrLog> listFiverrLog = new List<FiverrLog>();
        int listFiverrLogCount;
        int logPosition = -1;
        bool isLastLogUrl = false;
        string contentFiverrLog = string.Empty;
        string url = string.Empty;
        string log_gigID = string.Empty;
        int log_keywordID = 0;
        int log_fiverrID = 0;
        int isProcessed = 0;
        int log_ID = 0;
        string logRedirect = "https://block.fiverr.com/index.html?url=";
        bool isErrorLog = false;
        #endregion
        string[] gigCarNode = new string[] { };
        public Fiverr()
        {
            InitializeComponent();
        }

        public void GetLoadGig(string keyword)
        {
            var input = webBrowser1.Document.GetElementsByTagName("input");
            var button = webBrowser1.Document.GetElementsByTagName("button");
            foreach (HtmlElement element in input)
            {
                if (element.GetAttribute("type") == "search")
                {
                    element.Focus();
                    element.InnerText = keyword;
                }
            }
            foreach (HtmlElement element in button)
            {
                if (element.GetAttribute("className") == "submit-button btn-lrg-standard")
                {
                    if (element.OuterHtml.Contains("<button"))
                        element.Focus();
                    element.InvokeMember("click");
                    Application.DoEvents();
                }
            }
            Thread.Sleep(3000);
        }

        public void GetPageContent()
        {
            try
            {
                var divs = webBrowser1.Document.GetElementsByTagName("div");
                foreach (HtmlElement element in divs)
                {
                    if (element.GetAttribute("className") == "number-of-results")
                    {
                        ServicesAvailable = element.InnerHtml;
                    }
                    if (element.GetAttribute("className") == "listing-container")
                    {
                        SearchPageContent = element.InnerHtml;
                    }
                }
                //totalPage = Convert.ToInt32(Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage));
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "URL: " + webBrowser1.Url.AbsoluteUri, "GetPageContent");
            }
        }

        private void Fiverr_Load(object sender, EventArgs e)
        {
            //ScrapeFiverrDataDetails("5");
            //string cons = _scrapeHelper.GetSource("https://www.fiverr.com/bassammm/make-your-projects-and-assignments-in-python?context_referrer=search_gigs&source=top-bar&ref_ctx_id=ac36650d-0ca8-46d4-8e72-2c7c92d81898&pckg_id=1&pos=1");
            System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 4351");
            Thread.Sleep(2000);
            url = "http://www.fiverr.com";
            //string cont = _scrapeHelper.GetSource(url);
            webBrowser1.Navigate(url);

            lblGigDataStart.Visible = false;
            lblGigDataEnd.Visible = false;
            lblErrorMessage.Visible = false;
            #region datatable FiverrRawData columns
            dtFiverrRawData.TableName = "tbl_FiverrRawData";
            dtFiverrRawData.Columns.Add("FiverrDataID", typeof(int));
            dtFiverrRawData.Columns.Add("KeywordID", typeof(int));
            dtFiverrRawData.Columns.Add("GigID", typeof(string));
            dtFiverrRawData.Columns.Add("GigPosition", typeof(int));
            dtFiverrRawData.Columns.Add("GigIndex", typeof(int));
            dtFiverrRawData.Columns.Add("Page", typeof(int));
            dtFiverrRawData.Columns.Add("NextPageUrl", typeof(string));
            dtFiverrRawData.Columns.Add("ImageUrl", typeof(string));
            dtFiverrRawData.Columns.Add("ImageUrlSet", typeof(string));
            dtFiverrRawData.Columns.Add("StartingPrice", typeof(string));
            dtFiverrRawData.Columns.Add("CreatedOn", typeof(DateTime));
            dtFiverrRawData.Columns.Add("UpdatedOn", typeof(DateTime));
            dtFiverrRawData.Columns.Add("IsChecked", typeof(int));
            dtFiverrRawData.Columns.Add("IsRemoved", typeof(int));
            dtFiverrRawData.Columns.Add("IsGigProcessed", typeof(int));
            #endregion

            if (Environment.GetCommandLineArgs().Length > 0)
            {
                if (Environment.GetCommandLineArgs()[1].Contains("GroupName"))
                {
                    string[] arrGroup = Environment.GetCommandLineArgs()[1].Split(':');
                    GroupName = arrGroup[1].Trim(' ');
                }
                else
                {
                    if (Environment.GetCommandLineArgs()[1].Contains("KeywordID"))
                    {
                        string[] arrKeyword = Environment.GetCommandLineArgs()[1].Split(':');
                        keywordID = arrKeyword[1].Trim(' ');
                    }
                    else
                    {
                        keyword = Environment.GetCommandLineArgs()[1].Trim(' ');
                    }
                }
            }
            
            #region Get keyword processed or Not
            dtKeyword = _objFiverrBussiness.GetFiverrKeywordByGigProcessed(GroupName, keyword, keywordID);
            listKeyword = CommonUtility.ConvertToList<FiverrKeyword>(dtKeyword);
            keywordLength = listKeyword.Count;
            keywordPosition = 0;
            if (listKeyword.Count > 0)
            {
                if (keywordPosition < keywordLength)
                {
                    keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                    keyword= listKeyword[keywordPosition].KeywordName;
                    
                    lblGigDataStart.Visible = true;
                    lblGigDataStart.ForeColor = Color.Green;
                    lblGigDataStart.Text = "Fiverr Gig Data Scraping Started (" + keyword + "): " + DateTime.Now + "\n";
                    lblGigDataStart.Refresh();

                    FiverrKeyword data = new FiverrKeyword() { StartOn = DateTime.Now, Status = "Running", KeywordID = Convert.ToInt32(keywordID) };
                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);

                    string url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", keyword);
                   Content =  _scrapeHelper.GetSource(url);
                     //HtmlHelper.ReturnValue(con, "<button class=\"selection-trigger currency-selection-trigger text-body-2 lean-menu\">","<!-- -->")
                            //Currency = HtmlHelper.ReturnValue(Content, "<button class=\"selection-trigger currency-selection-trigger text-body-2 lean-menu\">", "</button>");
                    webBrowser1.Navigate(url);
                    isKeywordRestart = true;
                    isScrapGigDetails = false;
                    //isLastScrapGig = true;
                    if (!string.IsNullOrEmpty(GroupName)) { isKeywordGroup = true; }
                    else
                    {
                        keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                    }
                    //isKeywordGroup = true;
                }
            }
            if (!isKeywordRestart)
            {
                dtKeyword = _objFiverrBussiness.GetFiverrKeywordsByProccessed(GroupName, keyword, keywordID);
                listKeyword = CommonUtility.ConvertToList<FiverrKeyword>(dtKeyword);
                keywordLength = listKeyword.Count;
                keywordPosition = 0;
                if (listKeyword.Count > 0)
                {
                    if (keywordPosition < keywordLength)
                    {
                        string url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", listKeyword[keywordPosition].KeywordName);
                       Content =  _scrapeHelper.GetSource(url);
                        webBrowser1.Navigate(url);
                        isKeywordRestart = true;
                        isScrapGigDetails = true;
                        isLastScrapGig = true;
                        if (!string.IsNullOrEmpty(GroupName)) { isKeywordGroup = true; }
                        else
                        {
                            keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                        }
                        //isKeywordGroup = true;
                    }
                }
            }

            #endregion

            #region Fiverr Log table
            listFiverrLog = _objFiverrBussiness.GetFiverrDataDetailsLog();
            listFiverrLogCount = listFiverrLog.Count;
            #endregion

            if (listFiverrLogCount > 0)
            {
                logPosition = 0;
                url = listFiverrLog[logPosition].NextPageUrl;
                log_fiverrID = listFiverrLog[logPosition].FiverrDataID;
                log_keywordID = listFiverrLog[logPosition].KeywordID;
                log_gigID = listFiverrLog[logPosition].GigID;
                isProcessed = listFiverrLog[logPosition].IsProcessed;
                log_ID = listFiverrLog[logPosition].ID;
                isErrorLog = true;
                isScrapGigDetails = true;
                lblGigDataStart.Visible = true;
                lblGigDataStart.ForeColor = Color.Green;
                lblGigDataStart.Text = "Fiverr Gig Data Scraping Restart " + DateTime.Now + "\n";
                lblGigDataStart.Refresh();

                webBrowser1.Navigate(url);
            }
            else
            {
                if (!isKeywordRestart)
                {
                    //Get Keyword ID
                    if (!string.IsNullOrEmpty(keyword) || !string.IsNullOrEmpty(keywordID))
                    {
                        if (!string.IsNullOrEmpty(keyword))
                        {
                            keywordID = _objFiverrBussiness.IsKeywordExists(keyword, keywordID);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(keywordID))
                            {
                                keyword = _objFiverrBussiness.IsKeywordExists(keyword, keywordID);
                            }
                        }
                        if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(keywordID))
                        {
                            lblGigDataStart.Visible = true;
                            lblGigDataStart.ForeColor = Color.Green;
                            lblGigDataStart.Text = "Fiverr Gig Data Scraping Started (" + keyword + "): " + DateTime.Now + "\n";
                            lblGigDataStart.Refresh();

                            FiverrKeyword data = new FiverrKeyword() { StartOn = DateTime.Now, Status = "Running", KeywordID = Convert.ToInt32(keywordID) };
                            bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);

                            string url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", keyword);
                            Content =  _scrapeHelper.GetSource(url);
                            //HtmlHelper.ReturnValue(con, "<button class=\"selection-trigger currency-selection-trigger text-body-2 lean-menu\">","<!-- -->")
                            //Currency = HtmlHelper.ReturnValue(Content, "<button class=\"selection-trigger currency-selection-trigger text-body-2 lean-menu\">", "</button>");
                            webBrowser1.Navigate(url);
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = "No Keyword Schedule for today's.";
                            Thread.Sleep(3000);
                            Environment.Exit(0);
                        }
                    }
                    if (!string.IsNullOrEmpty(GroupName))
                    {
                        dtKeyword = _objFiverrBussiness.GetFiverrKeywords(GroupName);
                        listKeyword = CommonUtility.ConvertToList<FiverrKeyword>(dtKeyword);
                        keywordLength = listKeyword.Count;
                        if (listKeyword.Count > 0)
                        {
                            dtFiverrRawData.Rows.Clear();
                            keywordPosition = 0;
                            lblGigDataStart.Visible = true;
                            lblGigDataStart.ForeColor = Color.Green;
                            lblGigDataStart.Text = "Fiverr Gig Data Scraping Started (" + listKeyword[keywordPosition].KeywordName + "): " + DateTime.Now + "\n";
                            lblGigDataStart.Refresh();

                            keyword = listKeyword[keywordPosition].KeywordName;
                            keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                            dataExtractCount = listKeyword[keywordPosition].DataExtractCount;

                            FiverrKeyword data = new FiverrKeyword() { StartOn = DateTime.Now, Status = "Running", KeywordID = Convert.ToInt32(keywordID) };
                            bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);

                            string url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", keyword);
                           Content =  _scrapeHelper.GetSource(url);
                            webBrowser1.Navigate(url);
                            Thread.Sleep(2000);
                            isKeywordGroup = true;
                        }
                        else
                        {
                            lblErrorMessage.Visible = true;
                            lblErrorMessage.Text = "No Group Schedule for today's.";
                            Thread.Sleep(3000);
                            Environment.Exit(0);
                        }
                    }
                }
            }
        }
        private void fiverr_timer_Tick(object sender, EventArgs e)
        {
            //fiverr_timer.Stop();
            //System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 4351");
            //Thread.Sleep(3000);
            //webBrowser1.Navigate("http://www.fiverr.com");
        }
        void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                if (webBrowser1.ReadyState == WebBrowserReadyState.Complete)
                {
                    if (isErrorLog)
                    {
                        if (e.Url.AbsoluteUri.Contains("pos="))
                        {
                            contentFiverrLog = _scrapeHelper.GetSource(e.Url.AbsoluteUri);
                            if (!contentFiverrLog.Contains("307 Temporary Redirect"))
                            {
                                if (!string.IsNullOrEmpty(contentFiverrLog))
                                {
                                    GetScrapeFiverrDataDetails(contentFiverrLog, log_fiverrID.ToString(), log_gigID, log_keywordID.ToString());
                                    _objFiverrBussiness.UpdateFiverrDataDetailsLog(log_keywordID, log_fiverrID);
                                    logPosition++;
                                }
                                else
                                {
                                    FiverrLog log = new FiverrLog() { ID = log_ID, FiverrDataID = log_fiverrID, NextPageUrl = url, PageSource = contentFiverrLog, IsProcessed = 0, KeywordID = log_keywordID, GigID = log_gigID };
                                    _objFiverrBussiness.InsertUpdateFiverrDataDetailsLog(log);
                                    lblErrorMessage.Visible = true;
                                    lblErrorMessage.Text = "www.fiverr.com blocked. Please try again.";
                                    Thread.Sleep(3000);
                                    Application.Exit();
                                }
                            }
                            else
                            {
                                FiverrLog log = new FiverrLog() { ID = log_ID, FiverrDataID = log_fiverrID, NextPageUrl = url, PageSource = contentFiverrLog, IsProcessed = 0, KeywordID = log_keywordID, GigID = log_gigID };
                                _objFiverrBussiness.InsertUpdateFiverrDataDetailsLog(log);
                                lblErrorMessage.Visible = true;
                                lblErrorMessage.Text = "www.fiverr.com blocked. Please try again.";
                                Thread.Sleep(3000);
                                Application.Exit();
                            }
                        }
                        else
                        {
                            if (e.Url.AbsolutePath != "blank")
                            {
                                contentFiverrLog = _scrapeHelper.GetSource(e.Url.AbsoluteUri);
                                if (contentFiverrLog.Contains("<div class=\"inner flex-center max-width-container equal-padding\">"))
                                {
                                    FiverrLog log = new FiverrLog() { ID = log_ID, FiverrDataID = log_fiverrID, NextPageUrl = url, PageSource = contentFiverrLog, IsProcessed = 1, KeywordID = log_keywordID, GigID = log_gigID };
                                    _objFiverrBussiness.UpdateFiverrDataDetailsLog(log_keywordID, log_fiverrID);
                                    logPosition++;
                                }
                            }
                        }
                    }
                    if (e.Url.AbsoluteUri.Contains("https://block.fiverr.com/index.html?") || e.Url.AbsolutePath == "/index.html")
                    {
                        FiverrLog log = new FiverrLog() { ID = log_ID, FiverrDataID = log_fiverrID, NextPageUrl = url, PageSource = contentFiverrLog, IsProcessed = 0, KeywordID = log_keywordID, GigID = log_gigID };
                        _objFiverrBussiness.InsertUpdateFiverrDataDetailsLog(log);
                        lblErrorMessage.Visible = true;
                        lblErrorMessage.Text = "www.fiverr.com blocked. Please try again.";
                        Thread.Sleep(3000);
                        Application.Exit();
                    }
                    if (e.Url.AbsolutePath == "/")
                    {
                        if (isComplete)
                        {
                            Application.Exit();
                        }
                    }
                    if (!isScrapGigDetails && (e.Url.AbsolutePath == "/search/gigs" || e.Url.AbsolutePath.Contains("search")) || (!isScrapGigDetails && webBrowser1.Url.AbsolutePath == "/search/gigs"))
                    {
                        string pageLevel = string.Format("page={0}", page);
                        if (e.Url.AbsoluteUri.Contains(pageLevel) || webBrowser1.Url.AbsoluteUri.Contains(pageLevel))
                        {
                            GetPageContent();
                            if (e.Url.AbsolutePath == "blank")
                            {
                                ScrapFiverrRawData();
                                page++;
                                getNextPage(page);
                            }
                        }
                        else
                        {
                            GetPageContent();
                            if (e.Url.AbsolutePath == "blank")
                            {
                                ScrapFiverrRawData();
                                page++;
                                getNextPage(page);
                            }
                        }
                    }
                }
                if (isComplete && e.Url.AbsolutePath == "blank" && !isErrorLog)
                {
                    InsertFiverrRawData();
                    dtFiverrRawData.Rows.Clear();
                    if (isKeywordGroup)
                    {
                        isComplete = false;
                        keywordPosition++;
                        if (keywordPosition < keywordLength)
                        {
                            keyword = listKeyword[keywordPosition].KeywordName;
                            keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                            dataExtractCount = listKeyword[keywordPosition].DataExtractCount;
                            page = 1;

                            lblGigDataStart.Visible = true;
                            lblGigDataStart.ForeColor = Color.Green;
                            lblGigDataStart.Text = "Fiverr Gig Data Scraping Started (" + listKeyword[keywordPosition].KeywordName + "): " + DateTime.Now + "\n";
                            lblGigDataStart.Refresh();

                            FiverrKeyword data = new FiverrKeyword() { StartOn = DateTime.Now, Status = "Running", KeywordID = Convert.ToInt32(keywordID) };
                            bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);

                            string keywordUrl = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", keyword);
                            webBrowser1.Navigate(keywordUrl);
                        }
                        else
                        {
                            isLastScrapGig = true;
                            keywordPosition = 0;
                        }
                    }
                    else
                    {
                        isComplete = false;
                        isLastScrapGig = true;
                    }
                }

                if (isLastScrapGig && e.Url.AbsolutePath == "blank" && !isErrorLog)
                {
                    if (isKeywordGroup)
                    {
                        if (keywordPosition < keywordLength)
                        {
                            keywordID = listKeyword[keywordPosition].KeywordID.ToString();

                            lblGigDataStart.Visible = true;
                            lblGigDataStart.ForeColor = Color.Green;
                            lblGigDataStart.Text = "Fiverr Gig Details Scraping Start (" + listKeyword[keywordPosition].KeywordName + "): " + DateTime.Now + "\n";
                            lblGigDataStart.Refresh();

                            //Scrape Fiverr Data Details
                            ScrapeFiverrDataDetails(keywordID);
                            //Update Keyword table (endOn, status)
                            _objFiverrBussiness.GetKeywordRemark(Convert.ToInt32(keywordID), out Key_Remark, out Key_Status);
                            if (Key_Remark == "" && Key_Status == "Running")
                            {
                                DataTable dt = _objFiverrBussiness.GetDataExtractCountByStatus("Running", Convert.ToInt32(keywordID));
                                if (dt.Rows.Count > 0)
                                {
                                    foreach (DataRow dr in dt.Rows)
                                    {
                                        int dataExtractCount = (Convert.ToInt32(dr["DataExtractCount"].ToString()) + 1);
                                        FiverrKeyword data = new FiverrKeyword() { EndOn = DateTime.Now, Status = "Completed", KeywordID = Convert.ToInt32(keywordID), DataExtractCount = dataExtractCount, IsProcessed = 1, ScheduleOn = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") };
                                        bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);
                                        dtFiverrRawData.Rows.Clear();
                                        Key_Status = "Completed";
                                        Key_Remark = "";
                                    }
                                }
                            }
                            lblGigDataEnd.Visible = true;
                            lblGigDataEnd.ForeColor = Color.Green;
                            lblGigDataEnd.Text = "Fiverr Gig Details Scraping End (" + listKeyword[keywordPosition].KeywordName + "): " + DateTime.Now + "\n";
                            lblGigDataEnd.Refresh();

                            keywordPosition++;
                            if (keywordPosition != keywordLength)
                            {
                                url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", listKeyword[keywordPosition].KeywordName);
                               Content =  _scrapeHelper.GetSource(url);
                                webBrowser1.Navigate(url);
                            }
                            isScrapGigDetails = true;
                            if (keywordPosition == keywordLength)
                            {
                                isScrapLastKeyword = true;
                            }
                        }
                    }
                    else
                    {
                        lblGigDataStart.Visible = true;
                        lblGigDataStart.ForeColor = Color.Green;
                        lblGigDataStart.Text = "Fiverr Gig Details Scraping Start (" + keyword + "): " + DateTime.Now + "\n";
                        lblGigDataStart.Refresh();

                        //Scrape Fiverr Data Details
                        ScrapeFiverrDataDetails(keywordID);
                        //Update Keyword table (endOn, status)
                        _objFiverrBussiness.GetKeywordRemark(Convert.ToInt32(keywordID), out Key_Remark, out Key_Status);
                        if (Key_Remark == "" && Key_Status == "Running")
                        {
                            DataTable dt = _objFiverrBussiness.GetDataExtractCountByStatus("Running", Convert.ToInt32(keywordID));
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    int dataExtractCount = (Convert.ToInt32(dr["DataExtractCount"].ToString()) + 1);
                                    FiverrKeyword data = new FiverrKeyword() { EndOn = DateTime.Now, Status = "Completed", KeywordID = Convert.ToInt32(keywordID), DataExtractCount = dataExtractCount, IsProcessed = 1, ScheduleOn = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") };
                                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data, false);
                                    dtFiverrRawData.Rows.Clear();
                                    Key_Status = "Completed";
                                    Key_Remark = "";
                                }
                            }
                        }
                        lblGigDataEnd.Visible = true;
                        lblGigDataEnd.ForeColor = Color.Green;
                        lblGigDataEnd.Text = "Fiverr Gig Details Scraping  End (" + keyword + "): " + DateTime.Now + "\n";
                        lblGigDataEnd.Refresh();
                        //isScrapGigDetails = true;
                        isScrapLastKeyword = true;
                    }
                }
                if (isScrapLastKeyword && e.Url.AbsolutePath == "blank")
                {
                    #region Fiverr Log table
                    listFiverrLog = _objFiverrBussiness.GetFiverrDataDetailsLog();
                    listFiverrLogCount = listFiverrLog.Count;
                    #endregion

                    if (listFiverrLogCount > 0)
                    {
                        logPosition = 0;
                        url = listFiverrLog[logPosition].NextPageUrl;
                        log_fiverrID = listFiverrLog[logPosition].FiverrDataID;
                        log_keywordID = listFiverrLog[logPosition].KeywordID;
                        log_gigID = listFiverrLog[logPosition].GigID;
                        isProcessed = listFiverrLog[logPosition].IsProcessed;
                        log_ID = listFiverrLog[logPosition].ID;
                        isErrorLog = true;
                        isScrapGigDetails = true;
                        lblGigDataStart.Visible = true;
                        lblGigDataStart.ForeColor = Color.Green;
                        lblGigDataStart.Text = "Fiverr Gig Data Scraping Restart " + DateTime.Now + "\n";
                        lblGigDataStart.Refresh();

                        webBrowser1.Navigate(url);
                    }
                    else
                    {
                        bool isScheudleNext = Convert.ToBoolean(ConfigurationSettings.AppSettings["ScheduleForNext"].ToString());
                        if (isScheudleNext)
                        {
                            DateTime currentDate = DateTime.Now;
                            DateTime nextDayDate = currentDate.AddDays(1);
                            if (!string.IsNullOrEmpty(GroupName))
                            {
                                //Update Next Day  schedule
                                if (Key_Remark == "" && Key_Status == "Completed")
                                {
                                    FiverrKeyword data = new FiverrKeyword() { ScheduleOn = nextDayDate.ToString("yyyy-MM-dd"), IsProcessed = 0, IsGigProcessed = 0, GroupName = GroupName };
                                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data, true);
                                }
                            }
                            else
                            {
                                //Update Next Day  schedule
                                if (Key_Remark == "" && Key_Status == "Completed")
                                {
                                    FiverrKeyword data = new FiverrKeyword() { ScheduleOn = nextDayDate.ToString("yyyy-MM-dd"), IsProcessed = 0, IsGigProcessed = 0, KeywordName = keyword };
                                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data, true);
                                }
                            }
                            isLastKeyword = true;
                        }
                        Thread.Sleep(3000);
                        Environment.Exit(0);
                    }
                }
                if (logPosition < listFiverrLogCount && logPosition != -1 && e.Url.AbsolutePath == "blank")
                {
                    url = listFiverrLog[logPosition].NextPageUrl;
                    log_fiverrID = listFiverrLog[logPosition].FiverrDataID;
                    log_keywordID = listFiverrLog[logPosition].KeywordID;
                    log_gigID = listFiverrLog[logPosition].GigID;
                    isProcessed = listFiverrLog[logPosition].IsProcessed;
                    log_ID = listFiverrLog[logPosition].ID;
                    isErrorLog = true;
                    isScrapGigDetails = true;
                    webBrowser1.Navigate(url);
                }
                if (logPosition == listFiverrLogCount)
                {
                    listFiverrLog = _objFiverrBussiness.GetFiverrDataDetailsLog();
                    if (listFiverrLog.Count == 0)
                    {
                        DateTime currentDate = DateTime.Now;
                        DateTime nextDayDate = currentDate.AddDays(1);
                        DataTable dt = _objFiverrBussiness.GetDataExtractCountByStatus("Stop", 0);
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                if (!string.IsNullOrEmpty(GroupName))
                                {
                                    int dataExtractCount = (Convert.ToInt32(dr["DataExtractCount"].ToString()) + 1);
                                    FiverrKeyword data = new FiverrKeyword() { EndOn = DateTime.Now, Status = "Completed", DataExtractCount = dataExtractCount, Remark = "", ScheduleOn = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), KeywordID = Convert.ToInt32(dr["KeywordID"].ToString()), IsProcessed = 0, GroupName = GroupName };
                                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);
                                }
                                else
                                {
                                    int dataExtractCount = (Convert.ToInt32(dr["DataExtractCount"].ToString()) + 1);
                                    FiverrKeyword data = new FiverrKeyword() { EndOn = DateTime.Now, Status = "Completed", DataExtractCount = dataExtractCount, Remark = "", ScheduleOn = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), KeywordID = Convert.ToInt32(dr["KeywordID"].ToString()), IsProcessed = 0, KeywordName = keyword };
                                    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);
                                }
                            }
                            //foreach (DataRow dr in dt.Rows)
                            //{
                            //    int dataExtractCount = Convert.ToInt32(dr["DataExtractCount"].ToString());
                            //    FiverrKeyword data = new FiverrKeyword() { EndOn = DateTime.Now, Status = "Completed", DataExtractCount = dataExtractCount, Remark = "", ScheduleOn = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"), KeywordID = Convert.ToInt32(dr["KeywordID"].ToString()), IsProcessed = 0 };
                            //    bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data, true);
                            //}
                        }
                    }

                    lblGigDataEnd.Visible = true;
                    lblGigDataEnd.ForeColor = Color.Green;
                    lblGigDataEnd.Text = "Fiverr Gig Data Scraping End " + DateTime.Now + "\n";
                    lblGigDataEnd.Refresh();
                    isLastLogUrl = true;
                }
                if (isLastKeyword || isLastLogUrl)
                {
                    Thread.Sleep(2000);
                    Environment.Exit(0);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "URL: " + webBrowser1.Url.AbsoluteUri + "\n Keyword: " + keyword, "webBrowser1_Documented");
            }
        }
        public void getNextPage(int page)
        {
            if (Convert.ToInt32(ServicesAvailable) > 48)
            {
                string pages = (Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage, 1)).ToString();
                string[] arr = pages.Split('.');
                decimal val = 5;
                if(Convert.ToDecimal(arr[1])>=val)
                {
                    totalPage = Convert.ToInt32(Math.Round((Convert.ToDecimal(ServicesAvailable) / gigPerPage), 1));
                }
                else
                {
                    if (Convert.ToInt32(arr[1]) == 0)
                    {
                        totalPage = Convert.ToInt32(Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage));
                    }
                    else
                    {
                        totalPage = Convert.ToInt32(Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage)) + 1;
                    }
                }
                //totalPage = Convert.ToInt32(Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage)) + 1;
            }
            else
            {
                totalPage = Convert.ToInt32(Math.Round(Convert.ToDecimal(ServicesAvailable) / gigPerPage));
            }
            if (page <= totalPage)
            {
                string nextUrl = string.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}&page={1}", keyword, page);
                webBrowser1.Navigate(nextUrl);
                Thread.Sleep(3000);
            }
            else
            {
                isComplete = true;
            }
        }

        private void btnScrap_Click(object sender, EventArgs e)
        {
            try
            {

                if (isKeywordGroup)
                {
                    dtFiverrRawData.Rows.Clear();
                    if (listKeyword.Count > 0)
                    {
                        keywordPosition = 0;
                        lblGigDataStart.Visible = true;
                        lblGigDataStart.ForeColor = Color.Green;
                        lblGigDataStart.Text = "Fiverr Gig Data Scraping Started (" + listKeyword[keywordPosition].KeywordName + "): " + DateTime.Now + "\n";
                        lblGigDataStart.Refresh();

                        keyword = listKeyword[keywordPosition].KeywordName;
                        keywordID = listKeyword[keywordPosition].KeywordID.ToString();
                        dataExtractCount = listKeyword[keywordPosition].DataExtractCount;

                        FiverrKeyword data = new FiverrKeyword() { StartOn = DateTime.Now, Status = "Running", KeywordID = Convert.ToInt32(keywordID) };
                        bool result = _objFiverrBussiness.UpdateStatusOnKeyword(data);

                        string url = String.Format("https://www.fiverr.com/search/gigs?query={0}&source=main_banner&search_in=everywhere&search-autocomplete-original-term={0}", keyword);
                        webBrowser1.Navigate(url);
                        Thread.Sleep(2000);
                    }
                }
                else
                {
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = "There is no keyword schedule for today's.";
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "Scrap click");
            }
        }

        public void ScrapFiverrRawData()
        {
            try
            {
                if (!string.IsNullOrEmpty(SearchPageContent))
                {
                    int Position = 0;
                    ServicesAvailable = HtmlHelper.ReturnValue(ServicesAvailable, "", "Services available");
                    //Insert into FiverrKeyword table.
                    _objFiverrBussiness.InsertTotalServices(ServicesAvailable, Convert.ToInt32(keywordID));

                    string[] gigCarNode = SearchPageContent.Split(new string[] { "<div class=\"gig-card-layout\">" }, StringSplitOptions.None);
                    for (int k = 1; k < gigCarNode.Length; k++)
                    {
                        var gigSearchID = HtmlHelper.ReturnValue(gigCarNode[k], "data-gig-id=\"", "_");
                        //HtmlHelper.ReturnValue(gigCarNode[k], "data-gig-id=\"", "_\">");
                        //string[] gig = gigSearch.Split('_');
                        //var gigSearchID = gig[0];
                        //var Position = Convert.ToInt32(gig[1]);

                        var nextPageUrl = "https://www.fiverr.com" + HtmlHelper.ReturnValue(gigCarNode[k], "<a class=\"media\" href=\"", "\"");
                        var arr = nextPageUrl.Split(new string[] { "pos=" }, StringSplitOptions.None);
                        if (arr[1].Contains("&"))
                        {
                            var a = arr[1].Split('&');
                            Position = Convert.ToInt32(a[0]);
                        }
                        else
                        {
                            Position = Convert.ToInt32(arr[1]);
                        }
                        var startingPrice = HtmlHelper.ReturnValue(gigCarNode[k], "Starting at</small>", "</a></footer>");
                        var imageUrl = HtmlHelper.ReturnValue(gigCarNode[k], "", "src=\"", "\"");
                        var imageUrlSet = HtmlHelper.ReturnValue(gigCarNode[k], "", "srcSet=\"", "\"");
                        #region Rows added in Data table tbl_FiverrRawData
                        DataRow fiverrRawDataRow = dtFiverrRawData.NewRow();
                        fiverrRawDataRow = dtFiverrRawData.NewRow();
                        fiverrRawDataRow["FiverrDataID"] = 0;
                        fiverrRawDataRow["KeywordID"] = keywordID;
                        fiverrRawDataRow["GigID"] = gigSearchID;
                        fiverrRawDataRow["GigPosition"] = Position;
                        fiverrRawDataRow["GigIndex"] = k;
                        fiverrRawDataRow["Page"] = page;
                        fiverrRawDataRow["NextPageUrl"] = nextPageUrl;
                        fiverrRawDataRow["ImageUrl"] = imageUrl;
                        fiverrRawDataRow["ImageUrlSet"] = imageUrlSet;
                        fiverrRawDataRow["StartingPrice"] = startingPrice;
                        fiverrRawDataRow["CreatedOn"] = DateTime.Now;
                        fiverrRawDataRow["UpdatedOn"] = DateTime.Now;
                        fiverrRawDataRow["IsChecked"] = 1;
                        fiverrRawDataRow["IsRemoved"] = 0;
                        fiverrRawDataRow["IsGigProcessed"] = 1;
                        dtFiverrRawData.Rows.Add(fiverrRawDataRow);
                        #endregion
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Keyword: " + keyword, "ScrapFiverrRawData");
            }
        }

        public void InsertFiverrRawData()
        {
            try
            {
                DataTable dtFiverrRaw = _objFiverrBussiness.GetFiverrRawData(keywordID);
                if (dtFiverrRaw.Rows.Count == 0)
                {
                    _objFiverrBussiness.InsertFiverrRawData(dtFiverrRawData);
                }
                else
                {
                    //History Data fiverr_FiverrRawHistory
                    if (dtFiverrRaw.Rows.Count > 0)
                    {
                        List<FiverrData> listOldData = CommonUtility.ConvertToList<FiverrData>(dtFiverrRaw);
                        List<FiverrData> listNewData = CommonUtility.ConvertToList<FiverrData>(dtFiverrRawData);
                        foreach (var newData in listNewData)
                        {
                            var oldData = listOldData.Where(t => t.GigID.Contains(newData.GigID)).FirstOrDefault();
                            if (oldData != null)
                            {
                                if (oldData.GigID == newData.GigID)
                                {
                                    newData.FiverrDataID = oldData.FiverrDataID;
                                    InsertFiverrRawHistory(oldData, newData);
                                    newData.IsChecked = 1;
                                    newData.IsRemoved = 0;
                                    newData.IsGigProcessed = 1;
                                    _objFiverrBussiness.InsertUpdateFiverrRawData(newData);
                                }
                            }
                            else
                            {
                                //var res=  listOldData.Where(t => t.GigPosition == 33 && t.Page == 3).Select(t => new FiverrData { FiverrDataID = t.FiverrDataID, GigID = t.GigID, GigPosition = t.GigPosition });
                                newData.IsChecked = 1;
                                newData.IsRemoved = 0;
                                newData.IsGigProcessed = 1;
                                _objFiverrBussiness.InsertUpdateFiverrRawData(newData);
                            }
                        }
                        //Removed Gig 
                        DataTable dtGigIsChecked = _objFiverrBussiness.GetGigByChecked(keywordID);
                        if (dtGigIsChecked.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtGigIsChecked.Rows)
                            {
                                string isRemoved = dr["IsRemoved"] != null ? dr["IsRemoved"].ToString() : "";
                                string removedOn = dr["RemovedOn"] != null ? dr["RemovedOn"].ToString() : "";
                                if (isRemoved == "0" && removedOn == "")
                                {
                                    //Update all ischecked=0 rows by isRemoved.
                                    _objFiverrBussiness.UpdateGigByRemoved(dr["GigID"].ToString(), dr["KeywordID"].ToString());
                                    _objFiverrBussiness.InsertFiverrRawDataDeleted(dr);
                                }
                            }
                        }
                    }
                }
                //Gig update isGigProcessed true.
                FiverrKeyword obj = new FiverrKeyword() { IsGigProcessed = 1, KeywordID = Convert.ToInt32(keywordID) };
                _objFiverrBussiness.UpdateStatusOnKeyword(obj, false, true);
                //Gig Scrap End Here
                dtFiverrRawData.Rows.Clear();
                lblGigDataEnd.Visible = true;
                lblGigDataEnd.ForeColor = Color.Green;
                lblGigDataEnd.Text = "Fiverr Gig Data Scraping End (" + keyword + "): " + DateTime.Now + "\n";
                lblGigDataEnd.Refresh();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrRawData");
            }
        }
        public void InsertFiverrRawHistory(FiverrData oldData, FiverrData newData)
        {
            try
            {
                List<FiverrDataDetailsHistory> listHistory = new List<FiverrBAL.FiverrDataDetailsHistory>();
                FiverrDataDetailsHistory history = new FiverrBAL.FiverrDataDetailsHistory();

                if (oldData.GigID == newData.GigID)
                {

                    if (oldData.GigPosition != newData.GigPosition)
                    {
                        history.KeywordID = oldData.KeywordID;
                        history.FiverrDataID = oldData.FiverrDataID;
                        history.GigID = oldData.GigID;
                        history.ColumnName = "GigPosition";
                        history.ColumnValue = oldData.GigPosition.ToString();
                        _objFiverrBussiness.InsertFiverrRawHistory(history);
                    }
                    //if(oldData.Page!=newData.Page)
                    //{
                    //    history.KeywordID = oldData.KeywordID;
                    //    history.FiverrDataID = oldData.FiverrDataID;
                    //    history.GigID = oldData.GigID;
                    //    history.ColumnName = "Page";
                    //    history.ColumnValue = oldData.Page.ToString();
                    //    _objFiverrBussiness.InsertFiverrRawHistory(history);
                    //}
                    if (oldData.StartingPrice != newData.StartingPrice)
                    {
                        if (!string.IsNullOrEmpty(oldData.StartingPrice))
                        {
                            history.KeywordID = oldData.KeywordID;
                            history.FiverrDataID = oldData.FiverrDataID;
                            history.GigID = oldData.GigID;
                            history.ColumnName = "StartingPrice";
                            history.ColumnValue = oldData.StartingPrice;
                            _objFiverrBussiness.InsertFiverrRawHistory(history);
                        }
                    }
                    if (oldData.ImageUrlSet != newData.ImageUrlSet)
                    {
                        if (!string.IsNullOrEmpty(oldData.ImageUrlSet))
                        {
                            history.KeywordID = oldData.KeywordID;
                            history.FiverrDataID = oldData.FiverrDataID;
                            history.GigID = oldData.GigID;
                            history.ColumnName = "ImageUrlSet";
                            history.ColumnValue = oldData.ImageUrlSet;
                            _objFiverrBussiness.InsertFiverrRawHistory(history);
                        }
                    }
                    if (oldData.ImageUrl != newData.ImageUrl)
                    {
                        if (!string.IsNullOrEmpty(oldData.ImageUrl))
                        {
                            history.KeywordID = oldData.KeywordID;
                            history.FiverrDataID = oldData.FiverrDataID;
                            history.GigID = oldData.GigID;
                            history.ColumnName = "ImageUrl";
                            history.ColumnValue = oldData.ImageUrl;
                            _objFiverrBussiness.InsertFiverrRawHistory(history);
                        }
                    }
                    if (oldData.NextPageUrl != newData.NextPageUrl)
                    {
                        if (!string.IsNullOrEmpty(oldData.NextPageUrl))
                        {
                            history.KeywordID = oldData.KeywordID;
                            history.FiverrDataID = oldData.FiverrDataID;
                            history.GigID = oldData.GigID;
                            history.ColumnName = "NextPageUrl";
                            history.ColumnValue = oldData.NextPageUrl;
                            _objFiverrBussiness.InsertFiverrRawHistory(history);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrRawHistory");
            }
        }

        public void ScrapeFiverrDataDetails(string keywordID)
        {
            try
            {
                // Get Rows From fiverr Raw Data.
                DataTable dtFiverrData = new DataTable();
                dtFiverrData = _objFiverrBussiness.GetFiverrRawDataProcessed(keywordID);
                if (dtFiverrData.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtFiverrData.Rows)
                    {
                        string fiverrRawDataID = dr["FiverrDataID"].ToString();
                        string fiverrDataUrl = dr["NextPageUrl"].ToString();
                        string gigID = dr["GigID"].ToString();
                        string keywordId = dr["KeywordID"].ToString();
                        string fiverrDataDetailsID = string.Empty;
                        string fiverrDataContent = _scrapeHelper.GetSource(fiverrDataUrl);
                        try
                        {
                            if (fiverrDataContent != "")
                            {
                                string errorMessage = "307 Temporary redirect";
                                string message = HtmlHelper.ReturnValue(fiverrDataContent, "<title>", "</title>");
                                if (message.ToLower().Contains(errorMessage.ToLower()))
                                {
                                    FiverrLog log = new FiverrLog()
                                    {
                                        ID = 0,
                                        KeywordID = Convert.ToInt32(keywordId),
                                        FiverrDataID = Convert.ToInt32(fiverrRawDataID),
                                        GigID = gigID,
                                        NextPageUrl = fiverrDataUrl,
                                        PageSource = fiverrDataContent
                                    };
                                    _objFiverrBussiness.InsertUpdateFiverrDataDetailsLog(log);
                                    DataTable dt = _objFiverrBussiness.GetDataExtractCountByStatus("Running", Convert.ToInt32(keywordId));
                                    if (dt.Rows.Count > 0)
                                    {
                                        foreach (DataRow drRow in dt.Rows)
                                        {
                                            FiverrKeyword obj = new FiverrKeyword()
                                            {
                                                EndOn = DateTime.Now,
                                                Status = "Stop",
                                                KeywordID = Convert.ToInt32(keywordId),
                                                Remark = "Stop due to Captcha.",
                                                DataExtractCount = Convert.ToInt32((drRow["DataExtractCount"].ToString())) + 1,
                                            };
                                            _objFiverrBussiness.UpdateStatusOnKeyword(obj);
                                        }
                                    }
                                    //isErrorLog = true;
                                    continue;
                                }
                                else if (!fiverrDataContent.Contains("<!doctype html>"))
                                {
                                    if (fiverrDataContent.Contains("<html><body>You are being<a href=\"https://www.fiverr.com/categories/business\">redirected</a>.</body></html>") || fiverrDataContent.Contains("<html><body>You are being<a href=\"https://www.fiverr.com/categories/programming-tech\">redirected</a>.</body></html>") || fiverrDataContent.Contains("You are being"))
                                    {
                                        _objFiverrBussiness.UpdateGigByRemoved(gigID, keywordID,true);
                                        _objFiverrBussiness.UpdateSellerByRemoved(gigID, keywordId);
                                    }
                                }
                                else
                                {
                                    GetScrapeFiverrDataDetails(fiverrDataContent, fiverrRawDataID, gigID, keywordId);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonUtility.ErrorLog(ex, "FiverrRawDataID : " + fiverrRawDataID + " \n Url: " + fiverrDataUrl + "\n GigID: " + gigID + "\n FiverrDataDetailsID: " + fiverrDataDetailsID, "ScrapeFiverrDataDetails");
                        }
                        continue;
                    }
                    UpdateScrapeGigDetails(keywordID);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "ScrapeFiverrDataDetails", "941");
            }
        }

        public void GetScrapeFiverrDataDetails(string fiverrDataContent, string fiverrRawDataID, string gigID, string keywordId)
        {
            //Insert Fiverr Data details and get FiverrDataDetailsID
            string fiverrDataDetailsID = GetFiverrDataDetails(fiverrDataContent, keywordId, fiverrRawDataID, gigID);

            //Insert breadcrum details
            GetFiverrDetailsBreadcrumb(fiverrDataContent, fiverrDataDetailsID, gigID, keywordId);

            //Insert fiverr data details Images
            GetFiverrDataImages(fiverrDataContent, fiverrDataDetailsID, gigID, keywordId);

            //Insert Payment Details
            GetPaymentDetails(fiverrDataContent, keywordId, fiverrDataDetailsID, gigID);

            //Insert Fiverr Data Details FAQ
            GetFiverrDataFAQ(fiverrDataContent, fiverrDataDetailsID, gigID, keywordId);

            //Insert Fiverr Seller Details
            GetFiverrDataDetailsSeller(fiverrDataContent, fiverrRawDataID, fiverrDataDetailsID, gigID, keywordId);

            //Insert Review Data
            GetFiverrDataDetailsReview(fiverrDataContent, fiverrDataDetailsID, gigID, keywordId);

            _objFiverrBussiness.UpdateGigProcessed(gigID, fiverrRawDataID, keywordId);
            _objFiverrBussiness.UpdateGigDetailsprocessed(gigID, keywordId);
        }
        public void UpdateScrapeGigDetails(string keywordID)
        {
            try
            {
                _objFiverrBussiness.UpdateAllGigDetails(keywordID);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "UpdateScrapeGigDetails");
            }
        }
        #region Get Fiverr datat details and it's history
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        public string GetFiverrDataDetails(string fiverrDataContent, string keywordID, string fiverrRawDataID, string gigID)
        {
            string fiverrDataDetailsID = string.Empty, orderInQueue = string.Empty;
            int orderInQueueInNumber = 0;
            try
            {
                string toolCon = HtmlHelper.ReturnValue(fiverrDataContent, "<li class=\"metadata-attribute\">", "</ul>");
                string tool = HtmlHelper.ReturnValue(toolCon, "<li>", "</li>");

                orderInQueue = HtmlHelper.ReturnValue(fiverrDataContent, "<span class=\"orders-in-queue\">", "</span>");
                if (!string.IsNullOrEmpty(orderInQueue))
                {
                    string[] arr = orderInQueue.Split(' ');
                    orderInQueueInNumber = Convert.ToInt32(arr[0]);
                }
                FiverrDataDetails fiverrDataDetails = new FiverrBAL.FiverrDataDetails()
                {
                    FiverrDataID = Convert.ToInt32(fiverrRawDataID),
                    KeywordID = Convert.ToInt32(keywordID),
                    GigID = gigID,
                    Description = StripTagsRegex(HtmlHelper.ReturnValue(fiverrDataContent, "<div class=\"description-content\">", "<div class=\"cover\"></div>")),
                    Title = HtmlHelper.ReturnValue(fiverrDataContent, "<h1 class=\"text-display-3\">", "</h1>"),
                    YouSave = HtmlHelper.ReturnValue(fiverrDataContent, "<span class=\"collect-count\">", "</span>") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(fiverrDataContent, "<span class=\"collect-count\">", "</span>")) : 0,
                    OrderInQueue = orderInQueue,
                    OrderInQueueInNumber = orderInQueueInNumber,
                    Tool = tool != "" ? tool : ""
                };

                //Insert Data into History table
                DataTable dtFiverr = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_FiverrDataDetails");

                if (dtFiverr.Rows.Count > 0)
                {
                    List<FiverrDataDetails> oldFiverrDetails = CommonUtility.ConvertToList<FiverrDataDetails>(dtFiverr);

                    var oldData = oldFiverrDetails.Where(t => t.GigID == fiverrDataDetails.GigID).FirstOrDefault();
                    if (oldData != null)
                    {
                        if (oldData.GigID == fiverrDataDetails.GigID)
                        {
                            fiverrDataDetails.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                        }
                    }

                    InsertFiverrDataHistory(oldFiverrDetails, fiverrDataDetails);
                }
                else { fiverrDataDetails.FiverrDataDetailsID = 0; }
                fiverrDataDetailsID = _objFiverrBussiness.InsertFiverrDataDetails(fiverrDataDetails);

                return fiverrDataDetailsID;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "KeywordID: " + keywordID + "\r GigID: " + gigID + "\r FiverrDataID: " + fiverrRawDataID, "GetFiverrDataDetails");
            }
            return fiverrDataDetailsID;
        }
        public void InsertFiverrDataHistory(List<FiverrDataDetails> oldFiverrDetails, FiverrDataDetails fiverrDataDetails)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrBAL.FiverrDataDetailsHistory();
                if (oldFiverrDetails.Count > 0)
                {
                    foreach (var oldData in oldFiverrDetails)
                    {
                        if (oldData.GigID == fiverrDataDetails.GigID)
                        {
                            if (oldData.Title != fiverrDataDetails.Title)
                            {
                                if (!string.IsNullOrEmpty(oldData.Title))
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "Title";
                                    history.ColumnValue = oldData.Title;
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                            if (oldData.Description != fiverrDataDetails.Description)
                            {
                                if (!string.IsNullOrEmpty(oldData.Description))
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "Description";
                                    history.ColumnValue = oldData.Description;
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                            if (oldData.OrderInQueue != fiverrDataDetails.OrderInQueue)
                            {
                                if (!string.IsNullOrEmpty(oldData.OrderInQueue))
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "OrderInQueue";
                                    history.ColumnValue = oldData.OrderInQueue;
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                            if (oldData.YouSave != fiverrDataDetails.YouSave)
                            {
                                if (oldData.YouSave != 0)
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "YouSave";
                                    history.ColumnValue = oldData.YouSave.ToString();
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                            if (oldData.Tool != fiverrDataDetails.Tool)
                            {
                                if (!string.IsNullOrEmpty(oldData.Tool))
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "Tool";
                                    history.ColumnValue = oldData.Tool;
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                            if (oldData.OrderInQueueInNumber != fiverrDataDetails.OrderInQueueInNumber)
                            {
                                if (oldData.OrderInQueueInNumber != 0)
                                {
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.GigID = oldData.GigID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.ColumnName = "OrderInQueueInNumber";
                                    history.ColumnValue = oldData.OrderInQueueInNumber.ToString();
                                    _objFiverrBussiness.InsertFiverrDataDetailsHistory(history);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Fiver Gig Details: " + fiverrDataDetails + "\n DataTable rows: " + oldFiverrDetails.Count, "InsertFiverrDataHistory");
            }
        }
        #endregion

        #region Fiverr Data Breadcrumb and it's history
        public void GetFiverrDetailsBreadcrumb(string fiverrDataContent, string fiverrDataDetailsID, string gigID, string keywordID)
        {
            try
            {
                List<FiverrDataBreadcrumb> listBreadcrumb;
                string[] fiverrBreadcrumbs = fiverrDataContent.Split(new string[] { "<nav class=\"breadcrumbs text-body-2\">" }, StringSplitOptions.None);// document.DocumentNode.SelectNodes("//nav[contains(@class, 'breadcrumbs text-body-2')]").ToArray();
                int label = 1;
                listBreadcrumb = new List<FiverrDataBreadcrumb>();
                if (fiverrBreadcrumbs.Length > 0)
                {
                    for (int item = 1; item < fiverrBreadcrumbs.Length; item++)
                    {
                        string[] arr = fiverrBreadcrumbs[item].Split(new string[] { "</a>" }, StringSplitOptions.None);
                        for (int a = 0; a < arr.Length; a++)
                        {
                            if (arr[a].Contains("</nav>"))
                            {
                                break;
                            }
                            else
                            {
                                string[] res = arr[a].Split(new string[] { ">" }, StringSplitOptions.None);
                                string breadName = res[1];
                                if (breadName.Contains("&amp;"))
                                {
                                    breadName = breadName.Replace("&amp;", "&");
                                }
                                FiverrDataBreadcrumb breadcrumb = new FiverrBAL.FiverrDataBreadcrumb()
                                {
                                    ID = 0,
                                    FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                                    KeywordID = Convert.ToInt32(keywordID),
                                    BreadcrumbName = breadName,
                                    BreadcrumbUrl = HtmlHelper.ReturnValue(arr[a], "<a href=\"", "\""),
                                    Label = string.Format("Label{0}", label),
                                    GigID = gigID
                                };

                                label++;
                                listBreadcrumb.Add(breadcrumb);
                            }
                        }
                    }
                }
                DataTable dtBreadcrumb = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_FiverrDataDetailsBreadcrumb");
                if (dtBreadcrumb.Rows.Count > 0)
                {
                    List<FiverrDataBreadcrumb> oldBreadcrumb = CommonUtility.ConvertToList<FiverrDataBreadcrumb>(dtBreadcrumb);
                    foreach (var list in listBreadcrumb)
                    {
                        var oldData = oldBreadcrumb.Where(t => t.Label == list.Label && t.GigID == list.GigID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (oldData.Label == list.Label && oldData.GigID == list.GigID)
                            {
                                list.ID = oldData.ID;
                            }
                        }
                    }
                    InsertFiverrBreadcrumbHistory(oldBreadcrumb, listBreadcrumb);
                }
                _objFiverrBussiness.InsertFiverrDetailsBreadcrumb(listBreadcrumb);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrDetailsBreadcrumb");
            }
        }
        public void InsertFiverrBreadcrumbHistory(List<FiverrDataBreadcrumb> oldBreadcrumb, List<FiverrDataBreadcrumb> listBreadcrumb)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrBAL.FiverrDataDetailsHistory();
                if (listBreadcrumb.Count > 0)
                {
                    foreach (var breadcrumb in listBreadcrumb)
                    {
                        var oldData = oldBreadcrumb.Where(t => t.Label == breadcrumb.Label && t.GigID == breadcrumb.GigID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (oldData.Label == breadcrumb.Label && oldData.ID == breadcrumb.ID)
                            {
                                if (oldData.BreadcrumbName != breadcrumb.BreadcrumbName)
                                {
                                    if (!string.IsNullOrEmpty(oldData.BreadcrumbName))
                                    {
                                        history.BreadcrumbID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "BreadcrumbName";
                                        history.ColumnValue = oldData.BreadcrumbName;
                                        _objFiverrBussiness.InsertBreadcrumbHistory(history);
                                    }
                                }
                                if (oldData.BreadcrumbUrl != breadcrumb.BreadcrumbUrl)
                                {
                                    if (!string.IsNullOrEmpty(oldData.BreadcrumbUrl))
                                    {
                                        history.BreadcrumbID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "BreadcrumbUrl";
                                        history.ColumnValue = oldData.BreadcrumbUrl;
                                        _objFiverrBussiness.InsertBreadcrumbHistory(history);
                                    }
                                }
                            }
                        }
                    }
                    //_objFiverrBussiness.InsertBreadcrumbHistory(listHistory);//InsertFiverrDataDetailsHistory(listHistory);
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Breadcrumb list: " + listBreadcrumb.Count + "\n DataTable :" + oldBreadcrumb.Count, "InsertFiverrBreadcrumbHistory");
            }
        }
        #endregion

        #region Fiverr Data Images and it's history
        public void GetFiverrDataImages(string fiverrDataContent, string fiverrDataDetailsID, string gigID, string keywordID)
        {
            try
            {
                List<FiverrDataImages> listImages = new List<FiverrBAL.FiverrDataImages>();
                int count = 0;
                string imageUrl = string.Empty, videoUrl = string.Empty, imageUrlName = string.Empty, videoUrlName = string.Empty;
                string[] fiverrDataImages = fiverrDataContent.Split(new string[] { "<div class=\"slideshow-component slide-h do-not-animate\">" }, StringSplitOptions.None);

                if (fiverrDataImages.Length > 0)
                {
                    for (int item = 1; item < fiverrDataImages.Length; item++)
                    {
                        if (HtmlHelper.ReturnValue(fiverrDataImages[item], "", "src=\"", "\"").Contains("videos"))
                        {
                            videoUrlName = string.Format("VideoUrl{0}", (count == 0 ? "" : count.ToString()));
                            videoUrl = HtmlHelper.ReturnValue(fiverrDataImages[item], "", "src=\"", "\"");
                        }
                        else
                        {
                            imageUrlName = string.Format("ImageUrl{0}", (count == 0 ? "" : count.ToString()));
                            imageUrl = HtmlHelper.ReturnValue(fiverrDataImages[item], "", "src=\"", "\"");
                        }
                        FiverrDataImages images = new FiverrDataImages()
                        {
                            FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                            KeywordID = Convert.ToInt32(keywordID),
                            ImageUrlName = imageUrlName,//string.Format("ImageUrl{0}", (count == 0 ? "" : count.ToString())),
                            ImageUrl = imageUrl,//HtmlHelper.ReturnValue(fiverrDataImages[item], "", "src=\"", "\""),
                            VideoUrlName = videoUrlName,
                            VideoUrl = videoUrl,
                            GigID = gigID
                        };
                        listImages.Add(images);
                        count++;
                        string[] thumbNailImages = fiverrDataImages[item].Split(new string[] { "<a href=\"#\" class=\"thumbnail\"" }, StringSplitOptions.None);
                        if (thumbNailImages.Length > 0)
                        {
                            for (int it = 0; it < thumbNailImages.Length; it++)
                            {
                                FiverrDataImages image = new FiverrDataImages()
                                {
                                    FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                                    KeywordID = Convert.ToInt32(keywordID),
                                    ImageUrlName = string.Format("ImageUrl{0}", (count == 0 ? "" : count.ToString())),
                                    ImageUrl = HtmlHelper.ReturnValue(thumbNailImages[it], "", "src=\"", "\""),
                                    VideoUrlName = "",
                                    VideoUrl = "",
                                    GigID = gigID
                                };
                                listImages.Add(image);
                                count++;
                            }
                        }
                    }
                }

                DataTable dtImages = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_FiverrDataDetailsImages");
                if (dtImages.Rows.Count > 0)
                {
                    List<FiverrDataImages> oldImages = CommonUtility.ConvertToList<FiverrDataImages>(dtImages);
                    foreach (var list in listImages)
                    {
                        var oldData = oldImages.Where(t => t.ImageUrlName.Contains(list.ImageUrlName) && t.GigID == list.GigID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (list.ImageUrlName.Contains(oldData.ImageUrlName))
                            {
                                list.ID = oldData.ID;
                            }
                        }
                    }
                    InsertFiverrImagesHistory(oldImages, listImages);
                }
                _objFiverrBussiness.InsertFiverrDataImages(listImages);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Gigid: " + gigID, "InsertFiverrDataImages");
            }
        }

        public void InsertFiverrImagesHistory(List<FiverrDataImages> oldImages, List<FiverrDataImages> listImages)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrBAL.FiverrDataDetailsHistory();
                foreach (var image in listImages)
                {
                    if (image != null)
                    {
                        var oldData = oldImages.Where(t => (t.ImageUrlName.Contains(image.ImageUrlName)) && t.GigID == image.GigID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (oldData.ID == image.ID)
                            {
                                if (oldData.ImageUrlName != image.ImageUrlName)
                                {
                                    if (!string.IsNullOrEmpty(oldData.ImageUrlName))
                                    {
                                        history.ImageID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "ImageUrlName";
                                        history.ColumnValue = oldData.ImageUrlName;
                                        _objFiverrBussiness.InsertImagesHistory(history);
                                    }
                                }

                                if (oldData.ImageUrl != image.ImageUrl)
                                {
                                    if (!string.IsNullOrEmpty(oldData.ImageUrl))
                                    {
                                        history.ImageID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "ImageUrl";
                                        history.ColumnValue = oldData.ImageUrl;
                                        _objFiverrBussiness.InsertImagesHistory(history);
                                    }
                                }
                                if (oldData.VideoUrlName != image.VideoUrlName)
                                {
                                    if (!string.IsNullOrEmpty(oldData.VideoUrlName))
                                    {
                                        history.ImageID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "VideoUrlName";
                                        history.ColumnValue = oldData.VideoUrlName;
                                        _objFiverrBussiness.InsertImagesHistory(history);
                                    }
                                }

                                if (oldData.VideoUrl != image.VideoUrl)
                                {
                                    if (!string.IsNullOrEmpty(oldData.VideoUrl))
                                    {
                                        history.ImageID = oldData.ID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.GigID = oldData.GigID;
                                        history.ColumnName = "VideoUrl";
                                        history.ColumnValue = oldData.VideoUrl;
                                        _objFiverrBussiness.InsertImagesHistory(history);
                                    }
                                }
                            }
                        }
                    }
                }
                //_objFiverrBussiness.InsertImagesHistory(listHistory);//InsertFiverrDataDetailsHistory(listHistory);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Images List: " + listImages + "\n Datatable Rows: " + oldImages.Count, "InsertFiverrImagesHistory");
            }
        }
        #endregion

        #region Get Payment details And it's history
        public void GetPaymentDetails(string fiverrDataContent, string keywordID, string fiverrDataDetailsID, string gigID)
        {
            try
            {
                List<string> paymentOptionList = new List<string>();
                List<FiverrPayment> listPayment = new List<FiverrPayment>(); ;
                string[] fiverrDataDetailsPayment = fiverrDataContent.Split(new string[] { "<aside class=\"sidebar poly-sticky\">" }, StringSplitOptions.None); //document.DocumentNode.SelectNodes("//aside[contains(@class, 'sidebar poly-sticky')]").ToArray();
                string PaymentOption = string.Empty, revisions = string.Empty, title = string.Empty, price = string.Empty, description = string.Empty, deliveryTime = string.Empty;
                string deliveryTimeInHours = string.Empty;
                if (fiverrDataDetailsPayment.Length > 0)
                {
                    for (int item = 1; item < fiverrDataDetailsPayment.Length; item++)
                    {
                        if (fiverrDataDetailsPayment[item].Contains("<div class=\"packages-tabs single\">"))
                        {
                            paymentOptionList.Add("Form");
                        }
                        if (fiverrDataDetailsPayment[item].Contains("<div class=\"packages-tabs triple\">"))
                        {
                            string opt1, opt2, opt3 = string.Empty;
                            //<label for="package-tab-1" class="">Basic</label>
                            //HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-1\"", ">","</label>")
                            opt1 = HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-1\">", "</label>");
                            if (HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-1\">", "</label>") == "")
                            {
                                opt1 = HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-1\"", ">", "</label>");
                            }
                            opt2 = HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-2\">", "</label>");
                            if(opt2=="")
                            {
                                opt2= HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-2\"", ">", "</label>");
                            }
                            opt3 = HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-3\">", "</label>");
                            if(opt3=="")
                            {
                                opt3= HtmlHelper.ReturnValue(fiverrDataDetailsPayment[item], "<label for=\"package-tab-3\"", ">", "</label>");
                            }
                            paymentOptionList.Add(opt1);
                            paymentOptionList.Add(opt2);
                            paymentOptionList.Add(opt3);
                        }

                        string[] paymentOptionNodeDetails = fiverrDataDetailsPayment[item].Split(new string[] { "<div class=\"content\">" }, StringSplitOptions.None);
                        if (paymentOptionNodeDetails.Length > 0)
                        {
                            for (int i = 1; i < paymentOptionNodeDetails.Length; i++)
                            {
                                for (int j = 0; j < paymentOptionList.Count; j++)
                                {
                                    if (!string.IsNullOrEmpty(paymentOptionList[j]))
                                    {
                                        PaymentOption = paymentOptionList[j];
                                        paymentOptionList.RemoveAt(j);
                                        break;
                                    }
                                }

                                revisions = HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<b class=\"revisions\">", "</b>");
                                if (revisions.Contains("<!-- --><!-- -->"))
                                {
                                    revisions = revisions.Replace("<!-- --><!-- -->", " ");
                                }
                                title = HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<b class=\"title\">", "</b>") != "" ? HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<b class=\"title\">", "</b>") : "";
                                price = HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<span class=\"price\">", "</span>") != "" ? HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<span class=\"price\">", "</span>") : "";
                                if (price.Contains("â‚¹"))
                                {
                                    price = price.Replace("â‚¹", "₹");
                                }//₹
                                description = HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<p>", "</p>") != "" ? HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<p>", "</p>") : "";
                                deliveryTime = HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<b class=\"delivery\">", "</b>") != "" ? HtmlHelper.ReturnValue(paymentOptionNodeDetails[i], "<b class=\"delivery\">", "</b>") : "";
                                string[] delTime = deliveryTime.Split(' ');
                                int hours = 24;
                                deliveryTimeInHours = (Convert.ToInt32(delTime[0]) * hours).ToString();
                                string keyFeaturs = string.Empty;
                                string[] KeyFetureNode = paymentOptionNodeDetails[i].Split(new string[] { "<li " }, StringSplitOptions.None); //document.DocumentNode.SelectNodes("//ul[contains(@class,'features')]").ToArray();
                                if (KeyFetureNode.Length > 0)
                                {
                                    for (int k = 1; k < KeyFetureNode.Length; k++)
                                    {
                                        if (KeyFetureNode[k].Contains("class=\"feature included\">"))
                                        {
                                            keyFeaturs += HtmlHelper.ReturnValue(KeyFetureNode[k], "class=\"feature included\">", "</li>") + ",";
                                        }
                                        else { break; }
                                    }
                                }
                                FiverrPayment paymentDetails = new FiverrBAL.FiverrPayment()
                                {
                                    ID = 0,
                                    FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                                    KeywordID = Convert.ToInt32(keywordID),
                                    GigID = gigID,
                                    PaymentOption = PaymentOption,
                                    Title = title,
                                    Price = price,
                                    Description = description,
                                    DeliveryTime = deliveryTime,
                                    Revisions = revisions != "" ? revisions : "",
                                    KeyFeatures = keyFeaturs.TrimEnd(','),
                                    DeliveryTimeInHours = deliveryTimeInHours
                                };
                                listPayment.Add(paymentDetails);
                            }
                            DataTable dtPayment = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_fiverrDetailsPayment");
                            if (dtPayment.Rows.Count > 0)
                            {
                                List<FiverrPayment> oldPayment = CommonUtility.ConvertToList<FiverrPayment>(dtPayment);
                                foreach (var list in listPayment)
                                {
                                    var oldData = oldPayment.Where(t => t.PaymentOption == list.PaymentOption && t.GigID == list.GigID).FirstOrDefault();
                                    if (oldData != null)
                                    {
                                        if (oldData.PaymentOption == list.PaymentOption && oldData.GigID == list.GigID)
                                        {
                                            list.ID = oldData.ID;
                                        }
                                    }
                                }
                                InsertPaymentHistory(oldPayment, listPayment);
                            }
                            _objFiverrBussiness.InsertFiverrDataDetailsPayment(listPayment);
                        }
                    }
                }
                else
                {
                    CommonUtility.ErrorLog(null, "fiverrDataDetailsPayment : " + fiverrDataDetailsPayment.Length, "InsertPaymentDetails");
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertPaymentDetails");
            }
        }
        public void InsertPaymentHistory(List<FiverrPayment> oldPayment, List<FiverrPayment> listPayment)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrDataDetailsHistory();
                foreach (var payment in listPayment)
                {
                    var oldData = oldPayment.Where(t => t.PaymentOption == payment.PaymentOption && t.GigID == payment.GigID).FirstOrDefault();
                    if (oldData != null)
                    {
                        if (oldData.ID == payment.ID)
                        {
                            if (oldData.PaymentOption != payment.PaymentOption)
                            {
                                if (!string.IsNullOrEmpty(oldData.PaymentOption))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "PaymentOption";
                                    history.ColumnValue = oldData.PaymentOption;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.Title != payment.Title)
                            {
                                if (!string.IsNullOrEmpty(oldData.Title))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Title";
                                    history.ColumnValue = oldData.Title;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.Price != payment.Price)
                            {
                                if (!string.IsNullOrEmpty(oldData.Price))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Price";
                                    history.ColumnValue = oldData.Price;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.Description != payment.Description)
                            {
                                if (!string.IsNullOrEmpty(oldData.Description))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Description";
                                    history.ColumnValue = oldData.Description;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.DeliveryTime != payment.DeliveryTime)
                            {
                                if (!string.IsNullOrEmpty(oldData.DeliveryTime))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "DeliveryTime";
                                    history.ColumnValue = oldData.DeliveryTime;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.Revisions != payment.Revisions)
                            {
                                if (!string.IsNullOrEmpty(oldData.Revisions))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Revisions";
                                    history.ColumnValue = oldData.Revisions;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.KeyFeatures != payment.KeyFeatures)
                            {
                                if (!string.IsNullOrEmpty(oldData.KeyFeatures))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "KeyFeatures";
                                    history.ColumnValue = oldData.KeyFeatures;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                            if (oldData.DeliveryTimeInHours != payment.DeliveryTimeInHours)
                            {
                                if (!string.IsNullOrEmpty(oldData.DeliveryTimeInHours))
                                {
                                    history.PaymentID = oldData.ID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "DeliveryTimeInHours";
                                    history.ColumnValue = oldData.DeliveryTimeInHours;
                                    _objFiverrBussiness.InsertPaymentHistory(history);
                                }
                            }
                        }
                    }
                }
                //_objFiverrBussiness.InsertPaymentHistory(listHistory);//InsertFiverrDataDetailsHistory(listHistory);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Payment list: " + listPayment.Count + "\nDataTable: " + oldPayment.Count, "InsertPaymentHistory");
            }
        }
        #endregion

        #region Get Fiverr FAQ details and it's history
        public void GetFiverrDataFAQ(string fiverrDataContent, string fiverrDataDetailsID, string gigID, string keywordID)
        {
            try
            {
                List<FiverrFQA> listFAQ = new List<FiverrBAL.FiverrFQA>();
                string question = string.Empty, answer = string.Empty;
                string[] fiverrFAQNodes = fiverrDataContent.Split(new string[] { "<article" }, StringSplitOptions.None);//fiverrDataContent.Split(new string[] { "<div class=\"fit-collapsible-group\">" }, StringSplitOptions.None);// document.DocumentNode.SelectNodes("//div[contains(@class, 'fit-collapsible-group')]").ToArray();
                if (fiverrFAQNodes.Length > 0)
                {
                    for (int item = 1; item < fiverrFAQNodes.Length; item++)
                    {
                        if (fiverrFAQNodes[item].Contains("class=\"fit-collapsible\">"))
                        {
                            string[] questionNode = fiverrFAQNodes[item].Split(new string[] { "<div class=\"fit-collapsible-title\">" }, StringSplitOptions.None);
                            string[] answerNode = fiverrFAQNodes[item].Split(new string[] { "<div class=\"fit-collapsible-content\">" }, StringSplitOptions.None);
                            if (questionNode.Length > 0)
                            {

                                for (int i = 1; i < questionNode.Length; i++)
                                {
                                    question = HtmlHelper.ReturnValue(questionNode[i], "<p class=\"question\">", "</p>");

                                }
                                for (int j = 1; j < answerNode.Length; j++)
                                {
                                    answer = HtmlHelper.ReturnValue(answerNode[j], "<p class=\"answer\">", "</p>");
                                }
                                if (!string.IsNullOrEmpty(question) && !string.IsNullOrEmpty(answer))
                                {
                                    FiverrFQA faq = new FiverrBAL.FiverrFQA()
                                    {
                                        FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                                        KeywordID = Convert.ToInt32(keywordID),
                                        Question = question,
                                        Answer = answer,
                                        GigID = gigID
                                    };
                                    listFAQ.Add(faq);
                                }
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                DataTable dtFAQ = _objFiverrBussiness.GetFiverrGigFAQDetails(gigID);
                if (dtFAQ.Rows.Count > 0)
                {
                    List<FiverrFQA> oldFAQ = CommonUtility.ConvertToList<FiverrFQA>(dtFAQ);
                    foreach (var list in listFAQ)
                    {
                        var oldData = oldFAQ.Where(t => t.Question.Contains(list.Question) && t.GigID == list.GigID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (oldData.Question == list.Question)
                            {
                                list.QuestionID = oldData.QuestionID;
                            }
                        }
                    }
                    InsertFAQHistory(oldFAQ, listFAQ);
                }

                _objFiverrBussiness.InsertFiverrDataFAQ(listFAQ);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrDataFAQ");
            }
        }
        public void InsertFAQHistory(List<FiverrFQA> oldFAQ, List<FiverrFQA> listFAQ)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrDataDetailsHistory();
                foreach (var faq in listFAQ)
                {
                    var oldData = oldFAQ.Where(t => t.Question.Contains(faq.Question)).FirstOrDefault();
                    if (oldData != null)
                    {
                        if (oldData.QuestionID == faq.QuestionID)
                        {
                            if (oldData.Question != faq.Question)
                            {
                                if (!string.IsNullOrEmpty(oldData.Question))
                                {
                                    history.QuestionID = oldData.QuestionID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Question";
                                    history.ColumnValue = oldData.Question;
                                    _objFiverrBussiness.InsertFAQHistory(history);
                                }
                            }
                            if (oldData.Answer != faq.Answer)
                            {
                                if (!string.IsNullOrEmpty(oldData.Answer))
                                {
                                    history.QuestionID = oldData.QuestionID;
                                    history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                    history.KeywordID = oldData.KeywordID;
                                    history.GigID = oldData.GigID;
                                    history.ColumnName = "Answer";
                                    history.ColumnValue = oldData.Answer;
                                    _objFiverrBussiness.InsertFAQHistory(history);
                                }
                            }
                        }
                    }
                }
                //_objFiverrBussiness.InsertFAQHistory(listHistory);//InsertFiverrDataDetailsHistory(listHistory);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "FAQ List: " + listFAQ + "\nDataTable: " + oldFAQ.Count, "InsertFAQHistory");
            }
        }
        #endregion

        #region Seller information And it's History
        public void GetFiverrDataDetailsSeller(string fiverrDataContent, string fiverrRawDataID, string fiverrDataDetailsID, string gigID, string keywordID)
        {
            try
            {
                string totalReview = string.Empty, sellerName = string.Empty, profileURL = string.Empty, profileImageURL = string.Empty, ratingOutOfFive = string.Empty, totalRat = string.Empty, sellerLevel = string.Empty, title = string.Empty, sellerFrom = string.Empty, sellerMemberSince = string.Empty, averageResponseTime = string.Empty, description = string.Empty;
                string ratingOutFive = string.Empty;
                int totalRating = 0;
                string totatRatingAboutSeller = string.Empty;
                string averageResponseTimeInHours = string.Empty;
                int sellerLevelInNumber = 0;
                string[] sellerOverviewNode = fiverrDataContent.Split(new string[] { "<div class=\"seller-overview\">" }, StringSplitOptions.None); //document.DocumentNode.SelectNodes("//div[contains(@class,'seller-overview')]").ToArray();
                if (sellerOverviewNode.Length > 0)
                {
                    for (int item = 1; item < sellerOverviewNode.Length; item++)
                    {
                        sellerName = HtmlHelper.ReturnValue(sellerOverviewNode[item], "class=\"seller-link\">", "</a>");
                        profileURL = "https://www.fiverr.com" + HtmlHelper.ReturnValue(sellerOverviewNode[item], "<a href=\"", "\"");
                        profileImageURL = HtmlHelper.ReturnValue(sellerOverviewNode[item], "<img src=\"", "\"");
                        ratingOutOfFive = HtmlHelper.ReturnValue(sellerOverviewNode[item], "<span class=\"total-rating-out-five\">", "</span>");
                        ratingOutFive = ratingOutOfFive != "" ? ratingOutOfFive : "";
                        totalRat = HtmlHelper.ReturnValue(sellerOverviewNode[item], "<span class=\"total-rating\">(<!-- -->", "<!-- -->)</span>");
                        totalRating = totalRat != "" ? Convert.ToInt32(totalRat) : 0;
                        sellerLevel = HtmlHelper.ReturnValue(sellerOverviewNode[item], "class=\"seller-level hint hint--top\">", "</div>");//HtmlHelper.ReturnValue(sellerOverviewNode[item], "<p class=\"seller-level\"", "</p>");
                        if (!string.IsNullOrEmpty(sellerLevel))
                        {
                            string[] sellerLvl = sellerLevel.Split(' ');
                            sellerLevelInNumber = Convert.ToInt32(sellerLvl[1]);
                        }
                    }
                }
                else
                {
                    CommonUtility.ErrorLog(null, "Seller Overview: " + sellerOverviewNode.Length, "InsertFiverrDataDetailsSeller");
                }
                string[] sellerCardNode = fiverrDataContent.Split(new string[] { "<div class=\"seller-card\">" }, StringSplitOptions.None);
                if (sellerCardNode.Length > 0)
                {
                    for (int item = 1; item < sellerCardNode.Length; item++)
                    {
                        title = HtmlHelper.ReturnValue(sellerCardNode[item], "<div class=\"one-liner-rating-wrapper\"><small>", "</small>")!=""? HtmlHelper.ReturnValue(sellerCardNode[item], "<div class=\"one-liner-rating-wrapper\"><small>", "</small>") :"";
                        string review = HtmlHelper.ReturnValue(sellerCardNode[item], "</strong>(<!-- -->", "<!-- -->)</p>");
                        if (review.Contains("reviews") || review.Contains("review"))
                        {
                            review = review.Replace(" reviews", "").Replace(" review", "");
                        }
                        totalReview = review;
                        totatRatingAboutSeller = HtmlHelper.ReturnValue(sellerCardNode[item], "<p class=\"rating-text\"><strong>", "</strong>") != "" ? HtmlHelper.ReturnValue(sellerCardNode[item], "<p class=\"rating-text\"><strong>", "</strong>") : "0";
                    }
                }
                else
                {
                    CommonUtility.ErrorLog(null, "Seller card: " + sellerCardNode.Length, "InsertFiverrDataDetailsSeller");
                }
                string[] sellerdescriptionNode = fiverrDataContent.Split(new string[] { "<div class=\"stats-desc\">" }, StringSplitOptions.None);
                if (sellerdescriptionNode.Length > 0)
                {
                    for (int item = 1; item < sellerdescriptionNode.Length; item++)
                    {
                        sellerFrom = HtmlHelper.ReturnValue(sellerdescriptionNode[item], "<li>From<strong>", "</strong>");
                        sellerMemberSince = HtmlHelper.ReturnValue(sellerdescriptionNode[item], "<li>Member Since<strong>", "</strong>");
                        if (sellerMemberSince.Contains("<!-- --><!-- -->"))
                        {
                            sellerMemberSince = sellerMemberSince.Replace("<!-- --><!-- -->", " ");
                        }
                        averageResponseTime = HtmlHelper.ReturnValue(sellerdescriptionNode[item], "<li>Avg. Response Time<strong>", "</strong>");
                        if (!string.IsNullOrEmpty(averageResponseTime))
                        {
                            string[] avgTime = averageResponseTime.Split(' ');
                            averageResponseTimeInHours = avgTime[0];
                        }
                        description = HtmlHelper.ReturnValue(sellerdescriptionNode[item], "<div class=\"inner\">", "</div>");
                    }
                }
                else
                {
                    CommonUtility.ErrorLog(null, "stats-desc: " + sellerdescriptionNode.Length, "InsertFiverrDataDetailsSeller");
                }
                FiverrSeller seller = new FiverrSeller()
                {
                    SellerID = 0,
                    FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                    FiverrDataID = Convert.ToInt32(fiverrRawDataID),
                    KeywordID = Convert.ToInt32(keywordID),
                    SellerName = sellerName != "" ? sellerName : "",
                    ProfileURL = profileURL != "" ? profileURL : "",
                    ProfileImageURL = profileImageURL != "" ? profileImageURL : "",
                    Title = title != "" ? title : "",
                    Description = description != "" ? description : "",
                    SellerFrom = sellerFrom != "" ? sellerFrom : "",
                    SellerMemberSince = sellerMemberSince != "" ? sellerMemberSince : "",
                    AverageResponseTime = averageResponseTime != "" ? averageResponseTime : "",
                    RatingOutOfFive = ratingOutFive,
                    TotalRating = totalRating,
                    SellerLevel = sellerLevel != "" ? sellerLevel : "",
                    TotalReview = totalReview,
                    TotatRatingAboutSeller = totatRatingAboutSeller,
                    AverageResponseTimeInHours = averageResponseTimeInHours,
                    SellerLevelInNumber = sellerLevelInNumber,
                    GigID = gigID
                };

                DataTable dtSeller = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_Seller");
                if (dtSeller.Rows.Count > 0)
                {
                    List<FiverrSeller> oldSeller = CommonUtility.ConvertToList<FiverrSeller>(dtSeller);

                    var oldData = oldSeller.Where(t => t.GigID == seller.GigID).FirstOrDefault();
                    if (oldData != null)
                    {
                        if (oldData.GigID == seller.GigID)
                        {
                            seller.SellerID = oldData.SellerID;
                        }
                        seller.IsChecked = 1;
                        seller.IsRemoved = 0;
                        InsertSellerHistory(oldSeller, seller);
                    }
                    else
                    {
                        seller.IsChecked = 1;
                        seller.IsRemoved = 0;
                    }

                    var oldSellerData = oldSeller.Where(t => t.SellerName == seller.SellerName).FirstOrDefault();
                    if (oldSellerData == null)
                    {
                        //Maintain History removed seller.(fiverr_InsertFiverrSellerDeleted)
                        seller.IsChecked = 0;
                        seller.IsRemoved = 1;
                        _objFiverrBussiness.InsertSellerDeleted(seller);
                    }
                }

                _objFiverrBussiness.InsertFiverrDataDetailsSeller(seller);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrDataDetailsSeller");
            }
        }
        public void InsertSellerHistory(List<FiverrSeller> oldSeller, FiverrSeller NewSeller)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrDataDetailsHistory();
                if (oldSeller != null)
                {
                    foreach (FiverrSeller seller in oldSeller)
                    {
                        if (seller.GigID == NewSeller.GigID && seller.SellerID == NewSeller.SellerID)
                        {
                            if (seller.SellerName != NewSeller.SellerName)
                            {
                                if (!string.IsNullOrEmpty(seller.SellerName))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "SellerName";
                                    history.ColumnValue = seller.SellerName;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.ProfileURL != NewSeller.ProfileURL)
                            {
                                if (!string.IsNullOrEmpty(seller.ProfileURL))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "ProfileURL";
                                    history.ColumnValue = seller.ProfileURL;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.ProfileImageURL != NewSeller.ProfileImageURL)
                            {
                                if (!string.IsNullOrEmpty(seller.ProfileImageURL))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "ProfileImageURL";
                                    history.ColumnValue = seller.ProfileImageURL;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.Title != NewSeller.Title)
                            {
                                if (!string.IsNullOrEmpty(seller.Title))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "Title";
                                    history.ColumnValue = seller.Title;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.Description != NewSeller.Description)
                            {
                                if (!string.IsNullOrEmpty(seller.Description))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "Description";
                                    history.ColumnValue = seller.Description;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.SellerFrom != NewSeller.SellerFrom)
                            {
                                if (!string.IsNullOrEmpty(seller.SellerFrom))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "SellerFrom";
                                    history.ColumnValue = seller.SellerFrom;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.SellerMemberSince != NewSeller.SellerMemberSince)
                            {
                                if (!string.IsNullOrEmpty(seller.SellerMemberSince))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "SellerMemberSince";
                                    history.ColumnValue = seller.SellerMemberSince;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.AverageResponseTime != NewSeller.AverageResponseTime)
                            {
                                if (!string.IsNullOrEmpty(seller.AverageResponseTime))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "AverageResponseTime";
                                    history.ColumnValue = seller.AverageResponseTime;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.RatingOutOfFive != NewSeller.RatingOutOfFive)
                            {
                                if (seller.RatingOutOfFive != "")
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "RatingOutOfFive";
                                    history.ColumnValue = seller.RatingOutOfFive.ToString();
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.TotalRating != NewSeller.TotalRating)
                            {
                                if (seller.TotalRating != 0)
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "TotalRating";
                                    history.ColumnValue = seller.TotalRating.ToString();
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.SellerLevel != NewSeller.SellerLevel)
                            {
                                if (!string.IsNullOrEmpty(NewSeller.SellerLevel))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "SellerLevel";
                                    history.ColumnValue = seller.SellerLevel;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.TotalReview != NewSeller.TotalReview)
                            {
                                if (!string.IsNullOrEmpty(seller.TotalReview))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "TotalReview";
                                    history.ColumnValue = seller.TotalReview;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.TotatRatingAboutSeller != NewSeller.TotatRatingAboutSeller)
                            {
                                if (!string.IsNullOrEmpty(seller.TotatRatingAboutSeller))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "TotatRatingAboutSeller";
                                    history.ColumnValue = seller.TotatRatingAboutSeller;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.AverageResponseTimeInHours != NewSeller.AverageResponseTimeInHours)
                            {
                                if (!string.IsNullOrEmpty(seller.AverageResponseTimeInHours))
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "AverageResponseTimeInHours";
                                    history.ColumnValue = seller.AverageResponseTimeInHours;
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                            if (seller.SellerLevelInNumber != NewSeller.SellerLevelInNumber)
                            {
                                if (seller.SellerLevelInNumber != 0)
                                {
                                    history.SellerID = seller.SellerID;
                                    history.GigID = seller.GigID;
                                    history.FiverrDataDetailsID = seller.FiverrDataDetailsID;
                                    history.KeywordID = seller.KeywordID;
                                    history.ColumnName = "SellerLevelInNumber";
                                    history.ColumnValue = seller.SellerLevelInNumber.ToString();
                                    _objFiverrBussiness.InsertSellerHistory(history);
                                }
                            }
                        }
                    }
                }
                //_objFiverrBussiness.InsertSellerHistory(listHistory);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Seller List: " + NewSeller + " \nDataTable of Seller: " + oldSeller, "InsertSellerHistory");
            }
        }
        #endregion

        #region Review Information and it's history
        public void GetFiverrDataDetailsReview(string fiverrDataContent, string fiverrDataDetailsID, string gigID, string keywordID)
        {
            try
            {
                string[] fiverrReviewNode = fiverrDataContent.Split(new string[] { "<ul class=\"review-list\">" }, StringSplitOptions.None);//document.DocumentNode.SelectNodes("//ul[contains(@class, 'review-list')]").ToArray();
                List<FiverrReview> lstReview = new List<FiverrReview>();
                if (fiverrReviewNode.Length > 0)
                {
                    for (int item = 1; item < fiverrReviewNode.Length; item++)
                    {
                        string[] innerlist = fiverrReviewNode[item].Split(new string[] { "</li>" }, StringSplitOptions.None);
                        if (innerlist.Length > 0)
                        {
                            for (int it = 0; it < innerlist.Length; it++)
                            {
                                if (innerlist[it].Contains("<li class=\"review-item review-country\""))
                                {
                                    var imageUrl = HtmlHelper.ReturnValue(innerlist[it], "<img src=\"", "\"") != "" ? HtmlHelper.ReturnValue(fiverrReviewNode[item], "<img src=\"", "\"") : HtmlHelper.ReturnValue(fiverrReviewNode[item], "<span class=\"missing-image-user\">", "</span>");
                                    var name = HtmlHelper.ReturnValue(innerlist[it], "<div class=\"reviewer-details\"><h5>", "</h5>");
                                    var rating = HtmlHelper.ReturnValue(innerlist[it], "<span class=\"total-rating-out-five\">", "</span>");
                                    var country = HtmlHelper.ReturnValue(innerlist[it], "<div class=\"country-name font-accent\">", "</div>");
                                    var description = HtmlHelper.ReturnValue(innerlist[it], "<p class=\"text-body-2\">", "</p>");
                                    var thumbTitle = HtmlHelper.ReturnValue(innerlist[it], "<span class=\"thumb-title\">", "</span>");
                                    int help = 0;
                                    if (thumbTitle == "Helpful")
                                    {
                                        help = 1;
                                    }
                                    else if (thumbTitle == "Not Helpful")
                                    {
                                        help = 1;
                                    }
                                    var reviewDate = HtmlHelper.ReturnValue(innerlist[it], "<time class=\"text-body-2\">", "</time>");
                                    FiverrReview review = new FiverrReview()
                                    {
                                        ReviewID = 0,
                                        FiverrDataDetailsID = Convert.ToInt32(fiverrDataDetailsID),
                                        KeywordID = Convert.ToInt32(keywordID),
                                        CustomerImageUrl = imageUrl != "" ? imageUrl : "",
                                        CustomerName = name != "" ? name : "",
                                        ReviewRating = rating != "" ? rating : "",
                                        CustomerFrom = country != "" ? country : "",
                                        Description = description != "" ? description : "",
                                        IsHelpful = help,
                                        IsNotHelpful = help,
                                        ReviewDate = DateTime.Now,
                                        GigID = gigID
                                    };
                                    lstReview.Add(review);
                                }
                            }
                        }
                    }
                }
                DataTable dtReview = _objFiverrBussiness.GetFiverrGigDetails(gigID, keywordID, "tbl_FiverrCustomerReview");
                if (dtReview.Rows.Count > 0)
                {
                    List<FiverrReview> oldReview = CommonUtility.ConvertToList<FiverrReview>(dtReview);
                    foreach (var list in lstReview)
                    {
                        var oldData = oldReview.Where(t => t.CustomerName.Contains(list.CustomerName)).FirstOrDefault();
                        if (oldData != null)
                        {
                            list.ReviewID = oldData.ReviewID;
                        }
                    }
                    InsertFiverrReviewHistory(oldReview, lstReview);
                }
                _objFiverrBussiness.InsertFiverrReview(lstReview);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrDataDetailsReview");
            }
        }
        public void InsertFiverrReviewHistory(List<FiverrReview> oldReview, List<FiverrReview> lstReview)
        {
            try
            {
                FiverrDataDetailsHistory history = new FiverrDataDetailsHistory();
                if (lstReview.Count > 0)
                {
                    foreach (var review in lstReview)
                    {
                        var oldData = oldReview.Where(t => t.CustomerName.Contains(review.CustomerName) && t.ReviewID == review.ReviewID).FirstOrDefault();
                        if (oldData != null)
                        {
                            if (oldData.ReviewID == review.ReviewID)
                            {
                                if (oldData.CustomerName != review.CustomerName)
                                {
                                    if (!string.IsNullOrEmpty(oldData.CustomerName))
                                    {
                                        history.ReviewID = oldData.ReviewID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.GigID = oldData.GigID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.ColumnName = "CustomerName";
                                        history.ColumnValue = oldData.CustomerName;
                                        _objFiverrBussiness.InsertReviewHistory(history);
                                    }
                                }
                                if (oldData.CustomerImageUrl != review.CustomerImageUrl)
                                {
                                    if (!string.IsNullOrEmpty(oldData.CustomerImageUrl))
                                    {
                                        history.ReviewID = oldData.ReviewID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.GigID = oldData.GigID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.ColumnName = "CustomerImageUrl";
                                        history.ColumnValue = oldData.CustomerImageUrl;
                                        _objFiverrBussiness.InsertReviewHistory(history);
                                    }
                                }
                                if (oldData.CustomerFrom != review.CustomerFrom)
                                {
                                    if (!string.IsNullOrEmpty(oldData.CustomerFrom))
                                    {
                                        history.ReviewID = oldData.ReviewID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.GigID = oldData.GigID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.ColumnName = "CustomerFrom";
                                        history.ColumnValue = oldData.CustomerFrom;
                                        _objFiverrBussiness.InsertReviewHistory(history);
                                    }
                                }
                                if (oldData.Description.ToLower() != review.Description.ToLower())
                                {
                                    if (!string.IsNullOrEmpty(oldData.Description))
                                    {
                                        history.ReviewID = oldData.ReviewID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.GigID = oldData.GigID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.ColumnName = "Description";
                                        history.ColumnValue = oldData.Description;
                                        _objFiverrBussiness.InsertReviewHistory(history);
                                    }
                                }
                                if (oldData.ReviewRating != review.ReviewRating)
                                {
                                    if (oldData.ReviewRating != "")
                                    {
                                        history.ReviewID = oldData.ReviewID;
                                        history.FiverrDataDetailsID = oldData.FiverrDataDetailsID;
                                        history.GigID = oldData.GigID;
                                        history.KeywordID = oldData.KeywordID;
                                        history.ColumnName = "ReviewRating";
                                        history.ColumnValue = oldData.ReviewRating;
                                        _objFiverrBussiness.InsertReviewHistory(history);
                                    }
                                }
                            }
                        }
                    }
                }
                //_objFiverrBussiness.InsertReviewHistory(listHistory);//InsertFiverrDataDetailsHistory(listHistory);//InsertReviewHistory(listHistory);
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Review List: " + lstReview.Count + " \nDataTable of Review: " + oldReview.Count, "InsertFiverrReviewHistory");
            }
        }
        #endregion

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            webBrowser1.Refresh();
        }


    }
}
