﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Fiverr.FiverrBAL
{
    public class CommonUtility
    {
       static SqlConnection conFiverr = new SqlConnection(ConfigurationSettings.AppSettings["FiverrConnectionStringSql"].ToString());
        
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();
            try
            {
            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                        try
                        {
                            object value = dr[column.ColumnName];
                            if (value == DBNull.Value)
                                value = null;
                            if (pro.Name == column.ColumnName)
                                pro.SetValue(obj, value, null);
                        }
                        catch(Exception ex)
                        {
                            CommonUtility.ErrorLog(ex, "Pro name: "+pro.Name+"\t\t Datatabel column : "+dr[column.ColumnName], "GetItem");
                        }
                }
            }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "GetItem");
            }
            return obj;
        }
        public static List<T> ConvertToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        public static DataTable CreateToDataTable<T>(List<T> obj)
        {
            Type t = typeof(T);

            DataTable dt = new DataTable(t.Name);
            foreach (PropertyInfo pi in t.GetProperties())
            {

                dt.Columns.Add(new DataColumn(pi.Name));

            }
            if (obj != null)
            {

                foreach (T item in obj)
                {

                    DataRow dr = dt.NewRow();

                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc.ColumnName] = item.GetType().GetProperty(dc.ColumnName).GetValue(item, null);
                    }

                    dt.Rows.Add(dr);

                }
            }
            dt.AcceptChanges();
            return dt;
        }

        public static void ErrorLog(Exception exception, string message, string functionName)
        {
            string folderPath = ConfigurationSettings.AppSettings["ErrorLog"].ToString();
            string fileName = string.Format( "ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt");
            if(!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            if (!File.Exists(folderPath+"\\"+fileName))
            {
                using (FileStream fs = File.Create(folderPath + "\\" + fileName)) { }
            }

            using (StreamWriter writer = new StreamWriter(folderPath + "\\" + fileName, true))
            {

                if (exception != null)
                    writer.WriteLine("\r\nException: " + exception + "\r\n Message: " + message+"\r\n Function Name: "+functionName+"\r\n Date & Time: "+DateTime.Now);
                else
                    writer.WriteLine("\r\n Message: " + message,"\r\n Function Name: "+functionName + "\r\n Date & Time: " + DateTime.Now);

                writer.Close();
            }
        }

        public static void ErrorLog(Exception exception, string message, string functionName,string lineNumber)
        {
            //string folderPath = ConfigurationSettings.AppSettings["ErrorLog"].ToString();
            //string fileName = string.Format("ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt");
            //if (!Directory.Exists(folderPath))
            //{
            //    Directory.CreateDirectory(folderPath);
            //}
            //if (!File.Exists(folderPath + "\\" + fileName))
            //{
            //    using (FileStream fs = File.Create(folderPath + "\\" + fileName)) { }
            //}

            //using (StreamWriter writer = new StreamWriter(folderPath + "\\" + fileName, true))
            //{

            //    if (exception != null)
            //        writer.WriteLine("\r\nException: " + exception + "\r\n Message: " + message + "\r\n Function Name: " + functionName + "\r\n Date & Time: " + DateTime.Now);
            //    else
            //        writer.WriteLine("\r\n Message: " + message, "\r\n Function Name: " + functionName + "\r\n Date & Time: " + DateTime.Now);

            //    writer.Close();
            //}
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertErrorLog", conFiverr);
                cmd.Parameters.AddWithValue("@Exception", exception);
                cmd.Parameters.AddWithValue("@Message", message);
                cmd.Parameters.AddWithValue("@FunctionName", functionName);
                cmd.Parameters.AddWithValue("@LineNumber", lineNumber);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally { conFiverr.Close(); }
        }
    }
}
