﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Fiverr.FiverrBAL;

namespace Fiverr.FiverrBAL
{
    public class FiverrBussiness
    {

        SqlConnection conFiverr;
        public FiverrBussiness(string _conFiver)
        {
            conFiverr = new SqlConnection(_conFiver);
            if (conFiverr.State != ConnectionState.Closed)
            {
                conFiverr.Close();
            }
        }
        public DataTable GetFiverrKeywords(string groupName)
        {
            DataTable dt = new DataTable();
            try
            {
                //string query = string.Format("Select * from tbl_FiverrKeyword WHERE ScheduleOn='{0}' AND ", DateTime.Now.ToString("yyyy-MM-dd"));
                string query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND LOWER(grp.GroupName) = '{0}' AND grp.IsActive = 1 AND ky.ScheduleOn='{1}'", groupName.ToLower(), DateTime.Now.ToString("yyyy-MM-dd"));
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "GroupName: " + groupName, "GetFiverrKeywords");
            }
            finally { conFiverr.Close(); }
            return dt;
        }

        public DataTable GetFiverrKeywordByGigProcessed(string groupName,string keywordName,string keywordID)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = string.Empty;
                if (!string.IsNullOrEmpty(groupName))
                {
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND Lower(grp.GroupName) = '{0}' AND grp.IsActive = 1 AND ky.ScheduleOn='{1}' AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed') AND IsGigProcessed=0", groupName.ToLower(), DateTime.Now.ToString("yyyy-MM-dd"));
                }

                if (!string.IsNullOrEmpty(keywordName))
                {
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND Lower(ky.KeywordName) = '{0}' AND ky.ScheduleOn='{1}' AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed') AND IsGigProcessed=0", keywordName.ToLower(), DateTime.Now.ToString("yyyy-MM-dd"));
                }
                if (!string.IsNullOrEmpty(keywordID))
                {
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND ky.KeywordID = {0} AND ky.ScheduleOn='{1}' AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed')  AND IsGigProcessed=0", Convert.ToInt32(keywordID), DateTime.Now.ToString("yyyy-MM-dd"));
                }
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "GroupName: " + groupName, "GetFiverrKeywords");
            }
            finally { conFiverr.Close(); }
            return dt;
        }
        public DataTable GetFiverrKeywordsByProccessed(string groupName, string keywordName, string keywordID)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = string.Empty;
                if (!string.IsNullOrEmpty(groupName))
                {
                    //string query = string.Format("Select * from tbl_FiverrKeyword WHERE ScheduleOn='{0}' AND ", DateTime.Now.ToString("yyyy-MM-dd"));
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND Lower(grp.GroupName) = '{0}' AND grp.IsActive = 1 AND ky.ScheduleOn='{1}' AND IsProcessed=0 AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed') AND IsGigProcessed=1", groupName.ToLower(), DateTime.Now.ToString("yyyy-MM-dd"));
                }

                if (!string.IsNullOrEmpty(keywordName))
                {
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND Lower(ky.KeywordName) = '{0}' AND ky.ScheduleOn='{1}' AND IsProcessed=0 AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed') AND IsGigProcessed=1", keywordName.ToLower(), DateTime.Now.ToString("yyyy-MM-dd"));
                }
                if(!string.IsNullOrEmpty(keywordID))
                {
                    query = string.Format("select ky.KeywordID,ky.KeywordName,ky.DataExtractCount,ky.ServicesAvailable,ky.ScheduleOn,ky.StartOn,ky.EndOn,ky.Status,ky.Remark,grp.GroupName from tbl_FiverrKeyword ky INNER JOIN tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND ky.KeywordID = {0} AND ky.ScheduleOn='{1}' AND IsProcessed=0 AND DAY(ky.UpdatedOn)=DAY(SYSDATETIME()) AND (ky.Status!='Stop' OR ky.Status!='Completed')  AND IsGigProcessed=1", Convert.ToInt32(keywordID), DateTime.Now.ToString("yyyy-MM-dd"));
                }
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "GroupName: " + groupName, "GetFiverrKeywords");
            }
            finally { conFiverr.Close(); }
            return dt;
        }
        public void InsertTotalServices(string totalServices, int keywordID)
        {
            try
            {
                string _service = "";
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand();
                string serviceQuery = string.Format("Select ServicesAvailable FROM tbl_FiverrKeyword WHERE KeywordID={0}", keywordID);
                cmd.CommandText = serviceQuery;
                cmd.Connection = conFiverr;
                _service = cmd.ExecuteScalar()!=null?cmd.ExecuteScalar().ToString():"";
                cmd.Parameters.Clear();
                conFiverr.Close();
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                string query = string.Format("Update tbl_FiverrKeyword Set ServicesAvailable='{0}',UpdatedOn='{2}' WHERE KeywordID={1}", totalServices, keywordID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                cmd.CommandText = query;
                cmd.Connection = conFiverr;
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
                if (_service != totalServices)
                {
                    if (!string.IsNullOrEmpty(_service))
                    {
                        if (conFiverr.State != ConnectionState.Closed)
                        {
                            conFiverr.Close();
                        }
                        conFiverr.Open();
                        string queryHis = string.Format("insert into tbl_KeywordHistory values({0},'ServicesAvailable','{1}','{2}')", keywordID, _service, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        cmd.CommandText = queryHis;
                        cmd.Connection = conFiverr;
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "KeywordID: " + keywordID + " Total Services: " + totalServices, "InsertTotalServices");
            }
            finally { conFiverr.Close(); }
        }

        public DataTable GetDataExtractCountByStatus(string status, int keywordID)
        {
            DataTable dt = new DataTable();
            string query = string.Empty;
            try
            {
                if (status == "Running" && keywordID > 0)
                {
                    query = string.Format("Select * From tbl_FiverrKeyword WHERE Status='{0}' AND KeywordID={1}", status, keywordID);
                }
                else
                {
                    query = string.Format("Select * From tbl_FiverrKeyword WHERE Status='{0}'", status);
                }
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "GetDataExtractCountByStatus");
            }
            finally { conFiverr.Close(); }
            return dt;
        }
        public void GetKeywordRemark(int keywordID, out string remark, out string status)
        {
            remark = string.Empty;
            status = string.Empty;
            try
            {
                conFiverr.Open();
                string query = string.Format("Select Status,Remark FROM tbl_FiverrKeyword WHERE KeywordID={0}", keywordID);
                DataTable dt = ReturnDataTable(query);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        remark = dr["Remark"].ToString();
                        status = dr["Status"].ToString();
                    }
                }

                string queryUpdate = string.Format("UPDATE tbl_FiverrKeyword SET IsProcessed=1 WHERE KeywordID={0}", keywordID);
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(queryUpdate, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "KeywordID: " + keywordID, "GetKeywordRemark");
            }
            finally { conFiverr.Close(); }
        }

        public void UpdateFiverrDataDetailsLog(int log_keywordID, int log_fiverrID)
        {
            try
            {
                conFiverr.Open();
                string query = string.Format("UPDATE tbl_FiverrDataDetailsLog SET isProcessed=1,UpdatedOn='{2}' WHERE KeywordID={0} AND FiverrDataID={1}", log_keywordID, log_fiverrID, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "LogKeywordID: " + log_keywordID + " LogFiverrID: " + log_fiverrID, "UpdateFiverrDataDetailsLog");
            }
            finally { conFiverr.Close(); }
        }

        public bool UpdateStatusOnKeyword(FiverrKeyword obj, bool isLastKeyword = false,bool isGigProcessed=false)
        {
            try
            {
                string updateQuery = string.Empty;
                if (obj.StartOn != null && obj.Status == "Running")
                {
                    updateQuery = string.Format("UPDATE tbl_FiverrKeyword SET Status='{0}', StartOn='{1}' WHERE KeywordID={2}", obj.Status, obj.StartOn.ToString("yyyy-MM-dd HH:mm:ss"), obj.KeywordID);
                }
                if ((obj.EndOn != null && obj.Status == "Completed" ) || (obj.Status == "Stop"))
                {
                    if (!string.IsNullOrEmpty(obj.Remark))
                    {
                        updateQuery = string.Format("UPDATE tbl_FiverrKeyword SET Status='{0}', DataExtractCount={1},Remark='{2}' WHERE KeywordID={3}", obj.Status, obj.DataExtractCount, obj.Remark, obj.KeywordID);
                    }
                    else
                    {
                        updateQuery = string.Format("UPDATE tbl_FiverrKeyword SET Status='{0}', EndOn='{1}',DataExtractCount={2},ScheduleOn='{3}',Remark='',IsProcessed=1 WHERE KeywordID={4}", obj.Status, obj.EndOn.ToString("yyyy-MM-dd HH:mm:ss"), obj.DataExtractCount, obj.ScheduleOn, obj.KeywordID);
                    }
                }
                if (obj.ScheduleOn != null && isLastKeyword)
                {
                    if (!string.IsNullOrEmpty(obj.GroupName))
                    {
                        updateQuery = string.Format("UPDATE tbl_FiverrKeyword  SET tbl_FiverrKeyword.ScheduleOn='{0}',tbl_FiverrKeyword.IsProcessed=0,tbl_FiverrKeyword.IsGigProcessed=0 FROM tbl_FiverrKeyword ky inner join tbl_FiverrGroup grp ON ky.GroupID = grp.GroupID AND grp.GroupName = '{1}'", obj.ScheduleOn, obj.GroupName);
                    }
                    else
                    {
                        updateQuery = string.Format("UPDATE tbl_FiverrKeyword SET ScheduleOn='{0}',IsProcessed=0,IsGigProcessed=0 WHERE KeywordID={1} OR KeywordName='{2}'", obj.ScheduleOn, obj.KeywordID, obj.KeywordName);
                    }
                }
                if(obj.IsGigProcessed==1 && isGigProcessed)
                {
                    updateQuery = string.Format("UPDATE tbl_FiverrKeyword  SET tbl_FiverrKeyword.IsProcessed=0,tbl_FiverrKeyword.IsGigProcessed=1  WHERE KeywordID={0} OR KeywordName='{1}'", obj.KeywordID, obj.KeywordName);
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(updateQuery, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
                return true;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Fiverr Keyword Details: " + obj, "UpdateStatusOnKeyword");
            }
            finally { conFiverr.Close(); }

            return false;
        }

        public DataTable ReturnDataTable(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(query);
                cmd.Connection = conFiverr;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                conFiverr.Close();
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Query: " + query, "ReturnDataTable");
            }
            return dt;
        }
        public void InsertFiverrRawDataDeleted(DataRow deletedData)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertRemovedFiverrData", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@KeywordID", deletedData["KeywordID"]);
                cmd.Parameters.AddWithValue("@FiverrDataID", deletedData["FiverrDataID"]);
                cmd.Parameters.AddWithValue("@GigID", deletedData["GigID"]);
                cmd.Parameters.AddWithValue("@GigPosition", deletedData["GigPosition"]);
                cmd.Parameters.AddWithValue("@Page", deletedData["Page"]);
                cmd.Parameters.AddWithValue("@NextPageUrl", deletedData["NextPageUrl"]);
                cmd.Parameters.AddWithValue("@ImageUrl", deletedData["ImageUrl"]);
                cmd.Parameters.AddWithValue("@ImageUrlSet", deletedData["ImageUrlSet"]);
                cmd.Parameters.AddWithValue("@StartingPrice", deletedData["StartingPrice"]);
                cmd.Parameters.AddWithValue("@GigIndex", deletedData["GigIndex"]);
                cmd.Parameters.AddWithValue("@CreatedOn", deletedData["CreatedOn"]);
                cmd.Parameters.AddWithValue("@IsChecked", deletedData["IsChecked"]);
                cmd.Parameters.AddWithValue("@IsRemoved", deletedData["IsRemoved"]);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrRawDataDeleted");
            }
            finally { conFiverr.Close(); }
        }
        private int InsertBulkData(DataTable objData)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(conFiverr))
                {
                    foreach (DataColumn c in objData.Columns)
                        bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);

                    bulkCopy.DestinationTableName = objData.TableName;
                    try
                    {
                        bulkCopy.WriteToServer(objData);
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        CommonUtility.ErrorLog(ex, "", "InsertBulkData");
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertBulkData");
                return 0;
            }

            finally { conFiverr.Close(); }
        }

        public string IsKeywordExists(string keyword, string keywordId)
        {
            string keywordID = string.Empty;
            string keywordName = string.Empty;
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                if (!string.IsNullOrEmpty(keyword))
                {
                    string query = string.Format("select KeywordID from tbl_FiverrKeyword WHERE Lower(KeywordName)='{0}' AND ScheduleOn='{1}'", keyword.ToLower(),DateTime.Now.ToString("yyyy-MM-dd"));
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand(query, conFiverr);
                    var result = cmd.ExecuteScalar();
                    keywordID = result!=null?result.ToString():"";
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                    return keywordID;
                }
                if (!string.IsNullOrEmpty(keywordId))
                {
                    string query = string.Format("select KeywordName from tbl_FiverrKeyword WHERE KeywordID={0} AND ScheduleOn='{1}'", keywordId, DateTime.Now.ToString("yyyy-MM-dd"));
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand(query, conFiverr);
                    var result = cmd.ExecuteScalar();
                    keywordName = result != null ? result.ToString() : "";
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                    return keywordName;
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "KeywordName: " + keyword+"KeywordID: "+keywordId, "IsKeywordExists");
            }
            finally { conFiverr.Close(); }
            return "";
        }
        public void InsertFiverrRawData(DataTable fiverrRawData)
        {
            int Records = InsertBulkData(fiverrRawData);
        }
        public void InsertUpdateFiverrRawData(FiverrData newData)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertUpdateFiverrData", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@KeywordID", newData.KeywordID);
                cmd.Parameters.AddWithValue("@FiverrDataID", newData.FiverrDataID);
                cmd.Parameters.AddWithValue("@GigID", newData.GigID);
                cmd.Parameters.AddWithValue("@GigPosition", newData.GigPosition);
                cmd.Parameters.AddWithValue("@Page", newData.Page);
                cmd.Parameters.AddWithValue("@NextPageUrl", newData.NextPageUrl);
                cmd.Parameters.AddWithValue("@ImageUrl", newData.ImageUrl);
                cmd.Parameters.AddWithValue("@ImageUrlSet", newData.ImageUrlSet);
                cmd.Parameters.AddWithValue("@StartingPrice", newData.StartingPrice);
                cmd.Parameters.AddWithValue("@GigIndex", newData.GigIndex);
                cmd.Parameters.AddWithValue("@IsChecked", newData.IsChecked);
                cmd.Parameters.AddWithValue("@IsRemoved", newData.IsRemoved);
                cmd.Parameters.AddWithValue("@IsGigProcessed", newData.IsGigProcessed);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "UpdateFiverrRawData");
            }
            finally { conFiverr.Close(); }
        }
        public DataTable GetFiverrRawData(string keywordID)
        {
            DataTable dt = new DataTable();
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_GetFiverrRawData");
                cmd.Parameters.Add("@KeywordID", SqlDbType.Int).Value = Convert.ToInt32(keywordID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = conFiverr;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                conFiverr.Close();
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Gig search Keyword: " + keywordID + " \n Return data count: " + dt.Rows.Count, "GetFiverrRawData");
            }
            finally { conFiverr.Close(); }
            return dt;
        }

        public DataTable GetFiverrRawDataProcessed(string keywordID)
        {
            DataTable dt = new DataTable();
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                string query = string.Format("Select * From tbl_FiverrRawData Where KeywordID={0} AND IsChecked=1 AND IsRemoved=0 AND IsGigProcessed=1", Convert.ToInt32(keywordID));
                dt = ReturnDataTable(query);
                conFiverr.Close();
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Gig search Keyword: " + keywordID + " \n Return data count: " + dt.Rows.Count, "GetFiverrRawData");
            }
            finally { conFiverr.Close(); }
            return dt;
        }
        public DataTable GetGigByChecked(string keywordID)
        {
            DataTable dt = new DataTable();
            string query = string.Empty;
            try
            {
                //query = string.Format("Select * FROM tbl_FiverrRawData WHERE KeywordID={0} AND  (IsRemoved IS null OR IsRemoved=1 OR IsChecked=0)", Convert.ToInt32(keywordID));
                query = string.Format("Select * FROM tbl_FiverrRawData WHERE KeywordID={0} AND IsRemoved=0 AND IsGigProcessed=0 AND DAY(UpdatedOn)!=DAY(SYSDATETIME())", Convert.ToInt32(keywordID));
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Query: " + query, "GetGigByChecked");
            }
            finally { conFiverr.Close(); }
            return dt;
        }

        public void UpdateGigByRemoved(string GigID,string keywordID,bool isDetails=false)
        {
            string query = string.Empty;
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                if(isDetails)
                {
                    query = string.Format("Update tbl_FiverrRawData SET IsRemoved=1,RemovedOn='{0}',IsChecked=0,IsGigProcessed=0 WHERE GigID='{1}' AND KeywordID={2} AND IsGigProcessed=1", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), GigID, Convert.ToInt32(keywordID));
                }
                else
                {
                    query = string.Format("Update tbl_FiverrRawData SET IsRemoved=1,RemovedOn='{0}',IsChecked=0,IsGigProcessed=0 WHERE GigID='{1}' AND KeywordID={2} AND IsGigProcessed=0", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), GigID, Convert.ToInt32(keywordID));
                }
                //query = string.Format("Update tbl_FiverrRawData SET IsRemoved=1,RemovedOn='{0}',IsChecked=0,IsGigProcessed=0 WHERE GigID='{1}' AND KeywordID={2} AND IsGigProcessed=0", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), GigID,Convert.ToInt32(keywordID));
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Updated Gig Query: " + query, "UpdateGigByRemoved");
            }
        }

        public void UpdateSellerByRemoved(string GigID,string keywordID)
        {
            string query = string.Empty;
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                query = string.Format("Update tbl_Seller SET IsRemoved=1,RemovedOn='{0}',IsChecked=0 WHERE GigID='{1}' AND KeywordID={2}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), GigID,Convert.ToInt32(keywordID));
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Updated Gig Query: " + query, "UpdateGigByRemoved");
            }
        }
        public void InsertFiverrDetailsBreadcrumb(List<FiverrDataBreadcrumb> lstbreadcrumb)
        {
            try
            {
                foreach (var breadcrumb in lstbreadcrumb)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrBreadcrumbs", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", breadcrumb.ID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", breadcrumb.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@KeywordID", breadcrumb.KeywordID);
                    cmd.Parameters.AddWithValue("@BreadcrumbName", breadcrumb.BreadcrumbName);
                    cmd.Parameters.AddWithValue("@BreadcrumbUrl", breadcrumb.BreadcrumbUrl);
                    cmd.Parameters.AddWithValue("@Label", breadcrumb.Label);
                    cmd.Parameters.AddWithValue("@GigID", breadcrumb.GigID);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Breadcrumb List:" + lstbreadcrumb.ToList(), "InsertFiverrDetailsBreadcrumb");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertFiverrDataImages(List<FiverrDataImages> lstImages)
        {
            try
            {
                foreach (var images in lstImages)
                {
                    if (images != null)
                    {
                        if (conFiverr.State != ConnectionState.Closed)
                        {
                            conFiverr.Close();
                        }
                        conFiverr.Open();
                        SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrImages", conFiverr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID", images.ID);
                        cmd.Parameters.AddWithValue("@FiverrDataDetailsID", images.FiverrDataDetailsID);
                        cmd.Parameters.AddWithValue("@KeywordID", images.KeywordID);
                        cmd.Parameters.AddWithValue("@ImageUrlName", images.ImageUrlName);
                        cmd.Parameters.AddWithValue("@ImageUrl", images.ImageUrl);
                        cmd.Parameters.AddWithValue("@VideoUrlName", images.VideoUrlName);
                        cmd.Parameters.AddWithValue("@VideoUrl", images.VideoUrl);
                        cmd.Parameters.AddWithValue("@GigID", images.GigID);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        conFiverr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Images List: " + lstImages.ToList(), "InsertFiverrDataImages");
            }
            finally { conFiverr.Close(); }
        }
        public string InsertFiverrDataDetails(FiverrDataDetails details)
        {
            string FiverrDataDetailsID = string.Empty;
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrDetails", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@FiverrDataDetailID", SqlDbType.Int).Value = details.FiverrDataDetailsID;
                cmd.Parameters.Add("@FiverrDataID", SqlDbType.Int).Value = details.FiverrDataID;
                cmd.Parameters.Add("@KeywordID", SqlDbType.Int).Value = details.KeywordID;
                cmd.Parameters.Add("@GigID", SqlDbType.NVarChar).Value = details.GigID;
                cmd.Parameters.Add("@Title", SqlDbType.NVarChar).Value = details.Title;
                cmd.Parameters.Add("@Description", SqlDbType.NVarChar).Value = details.Description;
                cmd.Parameters.Add("@YouSave", SqlDbType.Int).Value = details.YouSave;
                cmd.Parameters.Add("@OrderInQueue", SqlDbType.NVarChar).Value = details.OrderInQueue;
                cmd.Parameters.Add("@Tool", SqlDbType.NVarChar).Value = details.Tool;
                cmd.Parameters.Add("@OrderInQueueInNumber", SqlDbType.Int).Value = details.OrderInQueueInNumber;
                cmd.Parameters.Add("@FiverrDataDetailsID", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                FiverrDataDetailsID = cmd.Parameters["@FiverrDataDetailsID"].Value.ToString();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "FiverrDataDetails: " + details, "InsertFiverrDataDetails");
            }
            finally { conFiverr.Close(); }
            return FiverrDataDetailsID;
        }

        public void InsertFiverrDataDetailsPayment(List<FiverrPayment> payment)
        {
            try
            {
                foreach (var item in payment)
                {
                    if (item != null)
                    {
                        if (conFiverr.State != ConnectionState.Closed)
                        {
                            conFiverr.Close();
                        }
                        conFiverr.Open();
                        SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrPayment", conFiverr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID", item.ID);
                        cmd.Parameters.AddWithValue("@FiverrDataDetailsID", item.FiverrDataDetailsID);
                        cmd.Parameters.AddWithValue("@KeywordID", item.KeywordID);
                        cmd.Parameters.AddWithValue("@GigID", item.GigID);
                        cmd.Parameters.AddWithValue("@PaymentOption", item.PaymentOption);
                        cmd.Parameters.AddWithValue("@Title", item.Title);
                        cmd.Parameters.AddWithValue("@Price", item.Price);
                        cmd.Parameters.AddWithValue("@Description", item.Description);
                        cmd.Parameters.AddWithValue("@DeliveryTime", item.DeliveryTime);
                        cmd.Parameters.AddWithValue("@Revisions", item.Revisions);
                        cmd.Parameters.AddWithValue("@KeyFeatures", item.KeyFeatures);
                        cmd.Parameters.AddWithValue("@DeliveryTimeInHours", item.DeliveryTimeInHours);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        conFiverr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Payment List: " + payment.ToList(), "InsertFiverrDataDetailsPayment");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertFiverrDataFAQ(List<FiverrFQA> lstfaq)
        {
            try
            {
                foreach (var faq in lstfaq)
                {
                    if (faq != null)
                    {
                        if (conFiverr.State != ConnectionState.Closed)
                        {
                            conFiverr.Close();
                        }
                        conFiverr.Open();
                        //SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrFAQ", conFiverr);
                        //cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.AddWithValue("@QuestionID", faq.QuestionID);
                        //cmd.Parameters.AddWithValue("@KeywordID", faq.KeywordID);
                        //cmd.Parameters.AddWithValue("@FiverrDataDetailsID", faq.FiverrDataDetailsID);
                        //cmd.Parameters.AddWithValue("@Question", faq.Question);
                        //cmd.Parameters.AddWithValue("@Answer", faq.Answer);
                        //cmd.Parameters.AddWithValue("@GigID", faq.GigID);
                        //cmd.ExecuteNonQuery();
                        //cmd.Parameters.Clear();
                        //conFiverr.Close();
                        string queryQuestion = string.Empty;
                        string queryAnswer = string.Empty;
                        if (faq.QuestionID > 0)
                        {

                            queryQuestion = string.Format("UPDATE tbl_FiverrDataDetailsQuestion SET Question='" + faq.Question + "',UpdatedOn='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE QuestionID=" + faq.QuestionID + " AND KeywordID=" + faq.KeywordID + " AND GigID='" + faq.GigID + "'");
                            using (SqlCommand cmd = new SqlCommand(queryQuestion, conFiverr))
                            {
                                cmd.ExecuteNonQuery();
                                if (faq.QuestionID > 0)
                                {
                                    queryAnswer = string.Format("UPDATE tbl_FiverrDataDetailsQAMapping SET Answer='" + faq.Answer + "',UpdatedOn='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE QuestionID=" + faq.QuestionID);
                                    using (SqlCommand cmd1 = new SqlCommand(queryAnswer, conFiverr))
                                    {
                                        cmd1.ExecuteNonQuery();
                                    }
                                }
                                cmd.Parameters.Clear();
                            }
                        }
                        else
                        {
                            queryQuestion = string.Format("INSERT INTO tbl_FiverrDataDetailsQuestion (FiverrDataDetailsID,KeywordID,GigID,Question,CreatedOn) VALUES(" + faq.FiverrDataDetailsID + "," + faq.KeywordID + ",'" + faq.GigID + "','" + faq.Question + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'); Select SCOPE_IDENTITY();");
                            using (SqlCommand cmd = new SqlCommand(queryQuestion, conFiverr))
                            {
                                cmd.ExecuteNonQuery();
                                int questionID = Convert.ToInt32(cmd.ExecuteScalar());
                                if (questionID > 0)
                                {
                                    queryAnswer = string.Format("INSERT INTO tbl_FiverrDataDetailsQAMapping (QuestionID,Answer,CreatedOn) VALUES(" + questionID + ",'" + faq.Answer + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
                                    using (SqlCommand cmd1 = new SqlCommand(queryAnswer, conFiverr))
                                    {
                                        cmd1.ExecuteNonQuery();
                                    }
                                }
                                cmd.Parameters.Clear();
                            }
                        }
                        conFiverr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "FAQ List: " + lstfaq.ToList(), "InsertFiverrDataFAQ");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertFiverrDataDetailsSeller(FiverrSeller seller)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrSeller", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SellerID", seller.SellerID);
                cmd.Parameters.AddWithValue("@FiverrDataID", seller.FiverrDataID);
                cmd.Parameters.AddWithValue("@FiverrDataDetailsID", seller.FiverrDataDetailsID);
                cmd.Parameters.AddWithValue("@KeywordID", seller.KeywordID);
                cmd.Parameters.AddWithValue("@SellerName", seller.SellerName);
                cmd.Parameters.AddWithValue("@ProfileURL", seller.ProfileURL);
                cmd.Parameters.AddWithValue("@ProfileImageURL", seller.ProfileImageURL);
                cmd.Parameters.AddWithValue("@Title", seller.Title);
                cmd.Parameters.AddWithValue("@Description", seller.Description);
                cmd.Parameters.AddWithValue("@SellerFrom", seller.SellerFrom);
                cmd.Parameters.AddWithValue("@SellerMemberSince", seller.SellerMemberSince);
                cmd.Parameters.AddWithValue("@AverageResponseTime", seller.AverageResponseTime);
                cmd.Parameters.AddWithValue("@RatingOutOfFive", seller.RatingOutOfFive);
                cmd.Parameters.AddWithValue("@TotalRating", seller.TotalRating);
                cmd.Parameters.AddWithValue("@SellerLevel", seller.SellerLevel);
                cmd.Parameters.AddWithValue("@TotalReview", seller.TotalReview);
                cmd.Parameters.AddWithValue("@GigID", seller.GigID);
                cmd.Parameters.AddWithValue("@TotatRatingAboutSeller", seller.TotatRatingAboutSeller);
                cmd.Parameters.AddWithValue("@AverageResponseTimeInHours", seller.AverageResponseTimeInHours);
                cmd.Parameters.AddWithValue("@SellerLevelInNumber", seller.SellerLevelInNumber);
                cmd.Parameters.AddWithValue("@IsChecked", seller.IsChecked);
                cmd.Parameters.AddWithValue("@IsRemoved", seller.IsRemoved);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Seller List: " + seller, "InsertFiverrDataDetailsSeller");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertFiverrReview(List<FiverrReview> lstReview)
        {
            try
            {
                foreach (var review in lstReview)
                {
                    if (review != null)
                    {
                        if (conFiverr.State != ConnectionState.Closed)
                        {
                            conFiverr.Close();
                        }
                        conFiverr.Open();
                        SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrReview", conFiverr);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReviewID", review.ReviewID);
                        cmd.Parameters.AddWithValue("@FiverrDataDetailsID", review.FiverrDataDetailsID);
                        cmd.Parameters.AddWithValue("@KeywordID", review.KeywordID);
                        cmd.Parameters.AddWithValue("@CustomerName", review.CustomerName);
                        cmd.Parameters.AddWithValue("@CustomerImageUrl", review.CustomerImageUrl);
                        cmd.Parameters.AddWithValue("@CustomerFrom", review.CustomerFrom);
                        cmd.Parameters.AddWithValue("@Description", review.Description);
                        cmd.Parameters.AddWithValue("@ReviewDate", review.ReviewDate);
                        cmd.Parameters.AddWithValue("@ReviewRating", review.ReviewRating);
                        cmd.Parameters.AddWithValue("@IsHelpful", review.IsHelpful);
                        cmd.Parameters.AddWithValue("@IsNotHelpful", review.IsNotHelpful);
                        cmd.Parameters.AddWithValue("@GigID", review.GigID);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        conFiverr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Review: " + lstReview, "InsertFiverrReview");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertSellerDeleted(FiverrSeller seller)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrSellerDeleted", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SellerID", seller.SellerID);
                cmd.Parameters.AddWithValue("@FiverrDataID", seller.FiverrDataID);
                cmd.Parameters.AddWithValue("@FiverrDataDetailsID", seller.FiverrDataDetailsID);
                cmd.Parameters.AddWithValue("@KeywordID", seller.KeywordID);
                cmd.Parameters.AddWithValue("@SellerName", seller.SellerName);
                cmd.Parameters.AddWithValue("@ProfileURL", seller.ProfileURL);
                cmd.Parameters.AddWithValue("@ProfileImageURL", seller.ProfileImageURL);
                cmd.Parameters.AddWithValue("@Title", seller.Title);
                cmd.Parameters.AddWithValue("@Description", seller.Description);
                cmd.Parameters.AddWithValue("@SellerFrom", seller.SellerFrom);
                cmd.Parameters.AddWithValue("@SellerMemberSince", seller.SellerMemberSince);
                cmd.Parameters.AddWithValue("@AverageResponseTime", seller.AverageResponseTime);
                cmd.Parameters.AddWithValue("@RatingOutOfFive", seller.RatingOutOfFive);
                cmd.Parameters.AddWithValue("@TotalRating", seller.TotalRating);
                cmd.Parameters.AddWithValue("@SellerLevel", seller.SellerLevel);
                cmd.Parameters.AddWithValue("@TotalReview", seller.TotalReview);
                cmd.Parameters.AddWithValue("@GigID", seller.GigID);
                cmd.Parameters.AddWithValue("@TotatRatingAboutSeller", seller.TotatRatingAboutSeller);
                cmd.Parameters.AddWithValue("@AverageResponseTimeInHours", seller.AverageResponseTimeInHours);
                cmd.Parameters.AddWithValue("@SellerLevelInNumber", seller.SellerLevelInNumber);
                cmd.Parameters.AddWithValue("@IsChecked", seller.IsChecked);
                cmd.Parameters.AddWithValue("@IsRemoved", seller.IsRemoved);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Seller List: " + seller, "InsertFiverrDataDetailsSeller");
            }
            finally { conFiverr.Close(); }
        }

        public void UpdateGigProcessed(string gigID,string fiverrRawDataID,string keywordID)
        {
            try
            {
                string query = string.Empty;
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                query = string.Format("UPDATE tbl_FiverrRawData SET IsGigProcessed=0 WHERE GigID='{0}' AND FiverrDataID={1} AND KeywordID={2}", gigID, Convert.ToInt32(fiverrRawDataID),Convert.ToInt32(keywordID));
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "GigID: "+gigID+"\t KeywordID: "+keywordID, "UpdateGigProcessed");
            }
            finally { conFiverr.Close(); }
        }
        public void UpdateGigDetailsprocessed(string gigID, string keywordID)
        {
            try
            {
                string query = string.Empty;
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                query = string.Format("UPDATE tbl_FiverrDataDetails SET IsGigDetail=1 WHERE GigID='{0}' AND KeywordID={1}", gigID, Convert.ToInt32(keywordID));
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "UpdateGigProcessed");
            }
            finally { conFiverr.Close(); }
        }

        public void UpdateAllGigDetails(string keywordID)
        {
            try
            {
                string query = string.Empty;
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                query = string.Format("UPDATE tbl_FiverrDataDetails SET IsGigDetail=0 WHERE KeywordID={0}", Convert.ToInt32(keywordID));
                SqlCommand cmd = new SqlCommand(query, conFiverr);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #region Insert Data In history Tables
        public void InsertFiverrRawHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_FiverrRawHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@FiverrDataID", history.FiverrDataID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Historical Data For FiverrDataDetailsHistory Table: " + history, "InsertFiverrDataDetailsHistory");
            }
            finally { conFiverr.Close(); }
        }

        public DataTable GetFiverrGigDetails(string gigID, string keywordId, string tableName)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = string.Empty;
                int keywordID = Convert.ToInt32(keywordId);
                if (tableName == "tbl_seller")
                {
                    query = string.Format("Select * From {0} Where GigID='{1}' AND KeywordID={2} AND IsChecked=1 AND IsRemoved=0", tableName, gigID, keywordID);
                }
                else
                {
                    query = string.Format("Select * From {0} Where GigID='{1}' AND KeywordID={2}", tableName, gigID, keywordID);
                }

                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "GetFiverrGigDetails");
            }
            finally { conFiverr.Close(); }
            return dt;
        }

        public DataTable GetFiverrGigFAQDetails(string gigID)
        {
            try
            {
                string query = string.Format("Select q.QuestionID,q.FiverrDataDetailsID,q.GigID,q.Question,qm.Answer From tbl_FiverrDataDetailsQuestion q INNER JOIN tbl_FiverrDataDetailsQAMapping qm ON q.QuestionID=qm.QuestionID Where GigID='{0}'", gigID);
                DataTable dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
            finally { conFiverr.Close(); }
        }
        public void InsertFiverrDataDetailsHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertFiverrDataDetailsHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Historical Data For FiverrDataDetailsHistory Table: " + history, "InsertFiverrDataDetailsHistory");
            }
            finally { conFiverr.Close(); }
        }
        public void InsertBreadcrumbHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertBreadcrumbHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BreadcrumbID", history.BreadcrumbID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Breadcrumb History List: " + history, "InsertBreadcrumbHistory");
            }
            finally { conFiverr.Close(); }
        }
        public void InsertImagesHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertImagesHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ImageID", history.ImageID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Images History List: " + history, "InsertImagesHistory");
            }
            finally { conFiverr.Close(); }
        }
        public void InsertPaymentHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertPaymentHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PaymentID", history.PaymentID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Payment History List: " + history, "InsertPaymentHistory");
            }
            finally { conFiverr.Close(); }
        }

        public void InsertFAQHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertFAQHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@QuestionID", history.QuestionID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "FAQ History List: " + history, "InsertFAQHistory");
            }
            finally { conFiverr.Close(); }
        }
        public void InsertSellerHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertSellerHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@SellerID", history.SellerID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Seller History List: " + history, "InsertSellerHistory");
            }
            finally { conFiverr.Close(); }
        }
        public void InsertReviewHistory(FiverrDataDetailsHistory history)
        {
            try
            {
                if (history != null)
                {
                    if (conFiverr.State != ConnectionState.Closed)
                    {
                        conFiverr.Close();
                    }
                    conFiverr.Open();
                    SqlCommand cmd = new SqlCommand("fiverr_InsertReviewHistory", conFiverr);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReviewID", history.ReviewID);
                    cmd.Parameters.AddWithValue("@FiverrDataDetailsID", history.FiverrDataDetailsID);
                    cmd.Parameters.AddWithValue("@GigID", history.GigID);
                    cmd.Parameters.AddWithValue("@KeywordID", history.KeywordID);
                    cmd.Parameters.AddWithValue("@ColumnName", history.ColumnName);
                    cmd.Parameters.AddWithValue("@ColumnValue", history.ColumnValue);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    conFiverr.Close();
                }
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "Review History List: " + history, "InsertReviewHistory");
            }
            finally { conFiverr.Close(); }
        }
        #endregion

        #region Error log
        public void InsertUpdateFiverrDataDetailsLog(FiverrLog log)
        {
            try
            {
                if (conFiverr.State != ConnectionState.Closed)
                {
                    conFiverr.Close();
                }
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand("fiverr_InsertUpdateFiverrLog", conFiverr);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", log.ID);
                cmd.Parameters.AddWithValue("@FiverrDataID", log.FiverrDataID);
                cmd.Parameters.AddWithValue("@KeywordID", log.KeywordID);
                cmd.Parameters.AddWithValue("@GigID", log.GigID);
                cmd.Parameters.AddWithValue("@NextPageUrl", log.NextPageUrl);
                cmd.Parameters.AddWithValue("@PageSource", log.PageSource);
                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                conFiverr.Close();
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "InsertFiverrDataDetailsLog");
            }
            finally { conFiverr.Close(); }
        }
        public List<FiverrLog> GetFiverrDataDetailsLog()
        {
            List<FiverrLog> list = new List<FiverrBAL.FiverrLog>();
            try
            {
                string query = string.Format("Select * from tbl_FiverrDataDetailsLog WHERE IsProcessed=0");
                DataTable dt = ReturnDataTable(query);

                list = CommonUtility.ConvertToList<FiverrLog>(dt);
                return list;
            }
            catch (Exception ex)
            {
                CommonUtility.ErrorLog(ex, "", "GetFiverrDataDetailsLog");
            }
            finally { conFiverr.Close(); }
            return list;
        }


        #endregion
    }
}
