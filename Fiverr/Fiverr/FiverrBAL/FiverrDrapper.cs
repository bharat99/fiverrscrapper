﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiverr.FiverrBAL
{
    public class FiverrDrapper
    {
        SqlConnection conFiverr;
        public FiverrDrapper(string _conFiverr)
        {
            conFiverr =new SqlConnection(_conFiverr);
        }
        public DataTable RunSpReturnsDt(string SpName,params SqlParameter[] parameter  )
        {
            try
            {
                DataTable dt = new DataTable();
                conFiverr.Open();
                SqlCommand cmd = new SqlCommand(SpName,conFiverr);
                cmd.Parameters.Add(parameter);
                int result=cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                }
                conFiverr.Close();
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
