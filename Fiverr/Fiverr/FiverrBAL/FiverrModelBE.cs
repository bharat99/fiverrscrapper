﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fiverr.FiverrBAL
{
    public class FiverrModelBE
    {
    }
    public class FiverrKeyword
    {
        public int KeywordID { get; set; }
        public string KeywordName { get; set; }
        public int DataExtractCount { get; set; }
        public string ServicesAvailable { get; set; }
        public string Status { get; set; }
        public string ScheduleOn { get; set; }
        public DateTime StartOn { get; set; }
        public DateTime EndOn { get; set; }
        public string Remark { get; set; }

        public int IsProcessed { get; set; }

        public string GroupName { get; set; }
        public int IsGigProcessed { get; set; }
    }
    public class FiverrData
    {
        public int FiverrDataID { get; set; }
        public int KeywordID { get; set; }
        public string GigID { get; set; }
        public int GigPosition { get; set; }
        public int Page { get; set; }
        public string NextPageUrl { get; set; }
        public string TotalServices { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSet { get; set; }
        public string StartingPrice { get; set; }
        public int GigIndex { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatededOn { get; set; }
        public int IsChecked { get; set; }
        public int IsRemoved { get; set; }
        public DateTime RemovedOn { get; set; }

        public int IsGigProcessed { get; set; }
    }
    public class FiverrDataDetails
    {
        public int FiverrDataID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string GigID { get; set; }
        public string Title { get; set; }
        public int YouSave { get; set; }
        public string OrderInQueue { get; set; }
        public string Description { get; set; }
        public string Tool { get; set; }
        public int OrderInQueueInNumber { get; set; }
    }

    public class FiverrDataDetailsHistory
    {
        public int ID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public string GigID { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

        public int SellerID { get; set; }
        public int ReviewID { get; set; }
        public int BreadcrumbID { get; set; }
        public int ImageID { get; set; }
        public int PaymentID { get; set; }
        public int QuestionID { get; set; }

        public int KeywordID { get; set; }

        public int FiverrDataID { get; set; }
    }

    public class FiverrDataBreadcrumb
    {
        public int ID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string BreadcrumbName { get; set; }
        public string BreadcrumbUrl { get; set; }
        public string Label { get; set; }
        public string GigID { get; set; }
    }

    public class FiverrDataImages
    {
        public int ID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string ImageUrlName { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrlName { get; set; }
        public string VideoUrl    { get; set; }
        public string GigID { get; set; }
    }

    public class FiverrPayment
    {
        public int ID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string GigID { get; set; }
        public string PaymentOption { get; set; }
        public string Title { get; set; }
        public string Price { get; set; }
        public string Description { get; set; }
        public string DeliveryTime { get; set; }
        public string DeliveryTimeInHours { get; set; }
        public string Revisions { get; set; }
        public string KeyFeatures { get; set; }
    }

    public class FiverrFQA
    {
        public int QuestionID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string GigID { get; set; }
    }

    public class FiverrSeller
    {
        public int SellerID { get; set; }
        public int FiverrDataID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string SellerName { get; set; }
        public string ProfileURL { get; set; }
        public string ProfileImageURL { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SellerFrom { get; set; }
        public string SellerMemberSince { get; set; }
        public string AverageResponseTime { get; set; }
        public string RatingOutOfFive { get; set; }
        public int TotalRating { get; set; }
        public string SellerLevel { get; set; }
        public string TotalReview { get; set; }
        public string TotatRatingAboutSeller { get; set; }
        public string AverageResponseTimeInHours { get; set; }

        public int SellerLevelInNumber { get; set; }
        public string GigID { get; set; }
        public int IsChecked { get; set; }
        public int IsRemoved{get;set;}
    }
    public class FiverrReview
    {
        public int ReviewID { get; set; }
        public int FiverrDataDetailsID { get; set; }
        public int KeywordID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerImageUrl { get; set; }
        public string CustomerFrom { get; set; }
        public string Description { get; set; }
        public DateTime ReviewDate { get; set; }
        public string ReviewRating { get; set; }
        public int IsHelpful { get; set; }
        public int IsNotHelpful { get; set; }
        public string GigID { get; set; }
    }
    public class FiverrLog
    {
        public int ID { get; set; }
        public int FiverrDataID { get; set; }
        public int KeywordID { get; set; }
        public string GigID { get; set; }
        public string NextPageUrl { get; set; }
        public string PageSource { get; set; }

        public int IsProcessed { get; set; }
    }

    public class tbl_FiverrRawDataDeleted
    {
        public int ID { get; set; }
        public int FiverrDataID { get; set; }
        public int KeywordID { get; set; }
        public string GigID { get; set; }
        public int GigPosition { get; set; }
        public int Page { get; set; }
        public string NextPageUrl { get; set; }
        public string TotalServices { get; set; }
        public string ImageUrl { get; set; }
        public string ImageUrlSet { get; set; }
        public string StartingPrice { get; set; }
        public int GigIndex { get; set; }
        public DateTime CreatedOn { get; set; }
        public int IsChecked { get; set; }
        public int IsRemoved { get; set; }
        public DateTime RemovedOn { get; set; }
    }
}
