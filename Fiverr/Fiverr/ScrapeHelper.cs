﻿using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Fiverr.FiverrBAL;
namespace Fiverr
{
    public class ScrapeHelper
    {
        int retryCount = 0;
        int ProxyChange = 0;
        //static int UserAgent = 0;
        //static string[] UserAgentArr = File.ReadAllLines(Path.GetFullPath("SupportFiles/UserAgents.txt"));
        public ScrapeHelper(int _retryCount)
        {
            retryCount = _retryCount;
        }
        public string GetSource(string url, string scookie = "")
        {
            string Content = String.Empty;
            int retry = 0;
            string myPageSource = string.Empty;
        retryContent:
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.MaxServicePointIdleTime = 0;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;

                if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                {
                    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                    if (ProxyChange >= 16)
                        ProxyChange = 0;
                }
                //myWebRequest.UserAgent = UserAgentArr[++UserAgent];
                //if (UserAgent >= UserAgentArr.Length - 2)
                //    UserAgent = 0;
                myWebRequest.UserAgent = "Mozilla/5.0(Windows NT 10.0; Win64; x64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36";
                //"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36"; 
                //"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
                myWebRequest.Referer = url;
                myWebRequest.Headers["Accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9,es;q=0.8";//"en-US,en;q=0.9";
                myWebRequest.ContentType = "application/json; charset=utf-8";//"text/html; charset=utf-8";
                myWebRequest.Headers["authority"] = "www.fiverr.com";
                myWebRequest.Headers["sec-fetch-dest"] = "empty";
                myWebRequest.Headers["sec-fetch-mode"] = "cors";//"navigate";
                myWebRequest.Headers["sec-fetch-site"] = "same-origin";//"cross-site";//"cross-site";
               // myWebRequest.Headers["Sec-Fetch-User"] = "?1";
               //myWebRequest.Headers["Cache-Control"] = "max-age=0";
               myWebRequest.Headers["Upgrade-Insecure-Requests"] = "1";

               // myWebRequest.Headers["Cookie"] = "u_guid=82ced67b-72f2-43f7-b684-92efdc7ad199; _fiverr_session_key=3f00dffbe349f5f8b10a9f5768c52807; _pxvid=e808d241-6e51-11ea-9c28-9bd3a2af1118; _gcl_au=1.1.1900504475.1585110869; _ga=GA1.2.1118226784.1585110870; _fbp=fb.1.1585110870783.732066965; ftr_ncd=6; G_ENABLED_IDPS=google; closed_cookie_approval=true; SL_C_23361dd035530_KEY=622c7c90ae62ff779a69fca685490fb34a70e5a9; hodor_creds=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTcxMTIxNDcsImlzcyI6ImZpdmVyci9ob2Rvci92MiIsInVpZCI6ODUzNzM5ODcsInVwcyI6IjlhMWRkMjM0Y2ZiMDE0ODA3NTdlMDUyZDY3NzQzMmI3NzM1YWVkNjJmYjgzOGM3Zjc0NDJhOWUxZjQ4MGYzMWU3Mjk2MTFiYzg3YzMxYjMwYmNhYjlmMDM1Njg1NGM0NDMxNTg4ZGUyOTU3NmViNzFiNzc1ZjJkN2UyOTRiZWE3IiwiaWF0IjoxNTg1NTc2MTQ3LCJzaWQiOiI2NTM0MGY4YzlhNzNmMzQzYTFkZDAxZTE3YTI2YmJjMCJ9.JLj7-S37xWkeBXY504OErL0XfQuHTvCSTXyFR55N_gA; ab.storage.userId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22somustar%22%2C%22c%22%3A1585576164670%2C%22l%22%3A1585576164670%7D; ab.storage.deviceId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22508cc5c4-c007-1a5b-9f70-d21f283f38ee%22%2C%22c%22%3A1585576164681%2C%22l%22%3A1585576164681%7D; QSI_SI_cOdEvSt56MEhQO1_intercept=true; QSI_SI_bxf9bbNwQ9iRLIF_intercept=true; SL_C_23361dd035530_VID=vBvo0FfO8H; __cfduid=db1dabc013035a20da7857806a4a9eab91590334752; logged_in_currency_v2=INR; session_referer=https%3A%2F%2Flocalhost%3A44348%2FDashboard; _gid=GA1.2.112157284.1590490354; searches-count=2; last_content_pages_=gigs%7C%7C%7Cshow%7C%7C%7C153387052%3Bgigs%7C%7C%7Cshow%7C%7C%7C146772803; QSI_HistorySession=https%3A%2F%2Fwww.fiverr.com%2Fbalintholecz%2Fresize-anything-with-artificial-intelligence-technology%3Fcontext_referrer%3Dsearch_gigs%26amp%3Bsource%3Dmain_banner%26amp%3Bref_ctx_id%3Dfa6adfd2-2f0e-4950-8487-38472f786437%26amp%3Bpckg_id%3D1%26amp%3Bpos%3D33~1590582736369%7Chttps%3A%2F%2Fwww.fiverr.com%2F~1590582782520; _pxhd=e1c3b7b4ccc2f9aaf7918dd46ffe6cf9c178136625cb487dd9537d2a72df37b1:e808d241-6e51-11ea-9c28-9bd3a2af1118; __cfruid=d60f0276bd782edb4ed9f1dd0c42dc0c91357309-1590647320; _px3=c2f50e7ec9eac706797c92e0bc961331d72402dbbd7ee801875fed016f363af2:Lw8qhfdqxIulSq7U4BfxqoQcLWs9w/N5UX3dwhU0CdwIiAevyoYdPhYOGcBXQ55ZDA+JobmF9K6UTbtqgScZCw==:1000:wOw4YoZjM0JhAoGGQSdf0OnE1YmEFBKbsLPYd5Cg7+7I+am70jyFce4HnDHIkzIAuCYB34B0DMqg4pWVW19Ba9qTNnJ1EhnHHbBSuSTSTQTNjRa9y3wL/6Po8uWki/BQR0Dq71AdUPCs/NoL2/uNA3nDL2RKQzLe8LT4q540m00=; pv_monthly=32%3B20%3B16; visited_fiverr=true; ftr_blst_1h=1590647382952; _uetsid=e41ed194-02dc-8005-f8f6-afad3cca09db; mp_436ab54ce79a37742241d4f156f647e9_mixpanel=%7B%22distinct_id%22%3A%2085373987%2C%22%24device_id%22%3A%20%22172509cd3f4780-0302509b1da4ca-f7d1d38-100200-172509cd3f5715%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Flocalhost%3A44348%2FDashboard%22%2C%22%24initial_referring_domain%22%3A%20%22localhost%3A44348%22%2C%22__mps%22%3A%20%7B%7D%2C%22__mpso%22%3A%20%7B%7D%2C%22__mpus%22%3A%20%7B%7D%2C%22__mpa%22%3A%20%7B%7D%2C%22__mpu%22%3A%20%7B%7D%2C%22__mpr%22%3A%20%5B%5D%2C%22__mpap%22%3A%20%5B%5D%2C%22%24user_id%22%3A%2085373987%7D; ab.storage.sessionId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22953a0efc-e9ec-54c5-33ad-329e8a060aa0%22%2C%22e%22%3A1590649360042%2C%22c%22%3A1590647381388%2C%22l%22%3A1590647560042%7D; forterToken=6a2408376047496ba71dcdb8058276d0_1590647558334__UDF43_9ck; _pxde=4366eb9b439197973f91839251248e8e134967cc42a006c85d768b74dc643fe7:eyJ0aW1lc3RhbXAiOjE1OTA2NDc2NTY4MTIsImZfa2IiOjAsImlwY19pZCI6W119";
                //"u_guid=82ced67b-72f2-43f7-b684-92efdc7ad199; _fiverr_session_key=3f00dffbe349f5f8b10a9f5768c52807; _pxvid=e808d241-6e51-11ea-9c28-9bd3a2af1118; _gcl_au=1.1.1900504475.1585110869; _ga=GA1.2.1118226784.1585110870; _fbp=fb.1.1585110870783.732066965; ftr_ncd=6; G_ENABLED_IDPS=google; closed_cookie_approval=true; SL_C_23361dd035530_KEY=622c7c90ae62ff779a69fca685490fb34a70e5a9; hodor_creds=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTcxMTIxNDcsImlzcyI6ImZpdmVyci9ob2Rvci92MiIsInVpZCI6ODUzNzM5ODcsInVwcyI6IjlhMWRkMjM0Y2ZiMDE0ODA3NTdlMDUyZDY3NzQzMmI3NzM1YWVkNjJmYjgzOGM3Zjc0NDJhOWUxZjQ4MGYzMWU3Mjk2MTFiYzg3YzMxYjMwYmNhYjlmMDM1Njg1NGM0NDMxNTg4ZGUyOTU3NmViNzFiNzc1ZjJkN2UyOTRiZWE3IiwiaWF0IjoxNTg1NTc2MTQ3LCJzaWQiOiI2NTM0MGY4YzlhNzNmMzQzYTFkZDAxZTE3YTI2YmJjMCJ9.JLj7-S37xWkeBXY504OErL0XfQuHTvCSTXyFR55N_gA; ab.storage.userId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22somustar%22%2C%22c%22%3A1585576164670%2C%22l%22%3A1585576164670%7D; ab.storage.deviceId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22508cc5c4-c007-1a5b-9f70-d21f283f38ee%22%2C%22c%22%3A1585576164681%2C%22l%22%3A1585576164681%7D; QSI_SI_cOdEvSt56MEhQO1_intercept=true; QSI_SI_bxf9bbNwQ9iRLIF_intercept=true; SL_C_23361dd035530_VID=vBvo0FfO8H; _pxhd=e1c3b7b4ccc2f9aaf7918dd46ffe6cf9c178136625cb487dd9537d2a72df37b1:e808d241-6e51-11ea-9c28-9bd3a2af1118; __cfduid=db1dabc013035a20da7857806a4a9eab91590334752; logged_in_currency_v2=INR; session_referer=https%3A%2F%2Flocalhost%3A44348%2FDashboard; _gid=GA1.2.112157284.1590490354; searches-count=2; last_content_pages_=gigs%7C%7C%7Cshow%7C%7C%7C151695964%3Bgigs%7C%7C%7Cshow%7C%7C%7C151139133; visited_fiverr=true; __cfruid=d7f546c6808e5bf50c93aba005ef6ce89b552031-1590578352; ftr_blst_1h=1590578361013; _px3=5b7ebdc46c43a171f9fc5976bfd09b5c9445bc1ef1bf29457d636ca177dfea93:WfZQyWwkE5sjiVpKX03iCUrt4BV40MP4EX+4Eeo5UyJn9AcjdT5ZDq4ASKvG/N4U7ONpfH+pbHU2X3iGBcqsfw==:1000:Pv6iZDWdt8XfXq14dOba0iVA3V0AvJ+F3sulgFc/1tHdQgM1vzAZZ9TNjcLep8Grr1mH/wi2ROIEO4GjiVCtwi9JZtPDTxo60ri/dnEEVBtKOvToBiAJoxUUBDvoqQBvnOjGJIcnxnTYpF4QPkAcuIKlCY8iQO+rC8RepP16Ysw=; _gat_UA-12078752-1=1; _uetsid=e41ed194-02dc-8005-f8f6-afad3cca09db; QSI_HistorySession=https%3A%2F%2Fwww.fiverr.com%2Fsandeep_malviya%2Fdo-microsoft-excel-macro-vba-function-formula-fast-automation-dashboard-etc%3Fcontext_referrer%3Dsearch_gigs%26amp%3Bsource%3Dmain_banner%26amp%3Bref_ctx_id%3D2a198c93-5e5f-4bde-ad5e-f18c66e0ba83%26amp%3Bpckg_id%3D1%26amp%3Bpos%3D25~1590578360554%7Chttps%3A%2F%2Fwww.fiverr.com%2F~1590578425335; ab.storage.sessionId.df88c069-84bb-4ae8-9830-0cfe6e341181=%7B%22g%22%3A%22b503b473-3c59-54b0-11da-81313c2b3a23%22%2C%22e%22%3A1590580225462%2C%22c%22%3A1590578360368%2C%22l%22%3A1590578425462%7D; forterToken=6a2408376047496ba71dcdb8058276d0_1590578424946__UDF43_9ck; mp_436ab54ce79a37742241d4f156f647e9_mixpanel=%7B%22distinct_id%22%3A%2085373987%2C%22%24device_id%22%3A%20%22172509cd3f4780-0302509b1da4ca-f7d1d38-100200-172509cd3f5715%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Flocalhost%3A44348%2FDashboard%22%2C%22%24initial_referring_domain%22%3A%20%22localhost%3A44348%22%2C%22__mps%22%3A%20%7B%7D%2C%22__mpso%22%3A%20%7B%7D%2C%22__mpus%22%3A%20%7B%7D%2C%22__mpa%22%3A%20%7B%7D%2C%22__mpu%22%3A%20%7B%7D%2C%22__mpr%22%3A%20%5B%5D%2C%22__mpap%22%3A%20%5B%5D%2C%22%24user_id%22%3A%2085373987%7D; pv_monthly=24%3B16%3B11; _pxde=b74768ae98f558802975be9329592d7a8da764ba1533617fec08e8e4b4955749:eyJ0aW1lc3RhbXAiOjE1OTA1Nzg0NDI3MjYsImZfa2IiOjAsImlwY19pZCI6W119";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                myWebRequest.Headers["x-csrf-token"] = "m1wsU+gEQTQm6vHieKnoWFe0SItp2zSgURTiGO7zl1Q="; 
               

                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;
                
                using (var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse())
                {
                    Stream responseStream = myWebResponse.GetResponseStream();
                    StreamReader myWebSource = new StreamReader(responseStream, Encoding.Default);
                     myPageSource = myWebSource.ReadToEnd();
                    string cookie = myWebResponse.Headers["Set-Cookie"];
                    string[] arrs = cookie.Split(';');
                    for (int i = 0; i < arrs.Length; i++)
                    {
                        if (arrs[i].Contains("expires".ToLower()))
                        {
                            if (arrs[i].Contains("-0000"))
                            {
                                int firstStringPosition = arrs[i].IndexOf("expires");
                                int secondStringPosition = arrs[i].IndexOf("-0000");
                                string stringBetweenTwoStrings = arrs[i].Substring(firstStringPosition, secondStringPosition - firstStringPosition + 0);
                                string[] a = stringBetweenTwoStrings.Split('=');
                                string da = a[1].TrimEnd(' ');
                                DateTime expireDate = Convert.ToDateTime(da);
                                DateTime currentDate = DateTime.Now;
                                if (expireDate.Date == currentDate.Date)
                                {
                                    Cookie c = new Cookie();
                                    c.Expires = expireDate.AddDays(1);
                                    myWebResponse.Cookies.Add(c);
                                    //CookieContainer co = new CookieContainer();
                                    //co.Add(c);
                                    //myWebRequest.CookieContainer = co;//CookieContainer.Add(c);
                                }
                            }
                        }
                    }
                }
                
                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(1000);
                    goto retryContent;
                }

                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("The server committed a protocol violation. Section=ResponseStatusLine".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("Received an unexpected EOF or 0 bytes from the transport stream.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The operation has timed out".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (408) Request Timeout.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if (ex.Message.ToLower().Contains("The remote server returned an error: (500) Internal Server Error.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else if(ex.Message.ToLower().Contains("Execution Timeout Expired.".ToLower()))
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                }
                else
                {
                    if (retry < retryCount)
                    {
                        retry++;
                        Thread.Sleep(1000);
                        goto retryContent;
                    }
                    else
                    {
                        CommonUtility.ErrorLog(ex, "Url : " + url, "GetSource");
                    }
                }
            }
            return myPageSource;
        }

        public string _GetJsonSource(string url, string cookieeee)
        {
            int retry = 0;   
            string myPageSource = "";
        retryContent:
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;
                myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                //myWebRequest.UserAgent = UserAgentArr[++UsrAgnt];
                //if (UsrAgnt >= UserAgentArr.Length - 2)
                //    UsrAgnt = 0;
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9";
                myWebRequest.Headers["Accept-Encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["Cookie"] = cookieeee;
                myWebRequest.Host = "www.fiverr.com";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;
                myWebRequest.KeepAlive = true;
                HttpWebResponse myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myWebSource = new StreamReader(responseStream, Encoding.UTF8);
                myPageSource = myWebSource.ReadToEnd();
                myWebResponse.Close();
                responseStream.Close();
                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(2000);
                    goto retryContent;
                }
            }
            catch (Exception ex)
            {
                goto retryContent;
            }
            return myPageSource;
        }



        public WebProxy SetProxySevere(int ChangeCount)
        {
            WebProxy setproxy = new WebProxy("world.proxymesh.com", 31280);
            if (ChangeCount <= 4)
            {
                setproxy = new WebProxy("us.proxymesh.com", 31280);
            }
            else
            {
                if (ChangeCount <= 8)
                {
                    setproxy = new WebProxy("us-il.proxymesh.com", 31280);
                }
                else
                {
                    if (ChangeCount <= 12)
                    {
                        setproxy = new WebProxy("us-fl.proxymesh.com", 31280);
                    }
                    else
                    {
                        if (ChangeCount <= 16)
                        {
                            setproxy = new WebProxy("us-ca.proxymesh.com", 31280);
                        }
                        else
                        {
                            setproxy = new WebProxy("us-dc.proxymesh.com", 31280);
                            ChangeCount = 0;
                        }
                    }
                }
            }

                return setproxy;
        }

        bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }
    }
}
